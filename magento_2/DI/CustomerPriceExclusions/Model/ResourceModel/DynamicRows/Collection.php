<?php

namespace DI\CustomerPriceExclusions\Model\ResourceModel\DynamicRows;



use  Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;



class Collection extends AbstractCollection

{

    protected $_idFieldName = 'row_id';



    protected function _construct()

    {

        $this->_init(

            'DI\CustomerPriceExclusions\Model\DynamicRows',

            'DI\CustomerPriceExclusions\Model\ResourceModel\DynamicRows'

        );

    }

}