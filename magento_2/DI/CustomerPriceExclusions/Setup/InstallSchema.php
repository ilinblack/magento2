<?php

namespace DI\CustomerPriceExclusions\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table as Table;


class InstallSchema implements InstallSchemaInterface

{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)

    {

        $installer = $setup;

        $installer->startSetup();



        $table = $installer->getConnection()->newTable(

            $installer->getTable('customer_price_exclusions')

        )->addColumn(

            'row_id',

            Table::TYPE_INTEGER,

            null,

            ['identity' => true, 'nullable' => false, 'primary' => true],

            'ID'

        )->addColumn(

            'brand',

            Table::TYPE_TEXT,

            null,

            ['nullable' => false],

            'Brand'

        )->addColumn(

            'customer_group',

            Table::TYPE_DATE,

            null,

            ['nullable' => false],

            'Customer Group'

        )->setComment(

            'DI Dynamic Rows'

        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();

    }

}