<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace DI\Documents\Model\Documents;

use DI\Documents\Model\ResourceModel\Documents\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataUploadProvider
 */
class DataUploadProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    public $_storeManager;
    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->_storeManager = $storeManager;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {

        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $currentStore = $this->_storeManager->getStore();

        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'/tmp/catalog/product';

        $doc = $this->getCollection()->getFirstItem();
        if ($doc) {
            $docData = $doc->getData();

            if(!empty($docData['path'])) {

                unset($docData['path']);

                $docData['path'][0]['name'] = $doc->getData('file_name');
                $docData['path'][0]['url'] = $mediaUrl.$doc->getData('path');
                $docData['path'][0]['size'] = $doc->getData('doc_size');
            }else{
                unset($docData['path']);
            }

            $this->loadedData[$doc->getId()] = $docData;
        }
        return $this->loadedData;
    }
}
