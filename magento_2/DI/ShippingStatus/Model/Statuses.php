<?php

namespace DI\ShippingStatus\Model;

class Statuses
{

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource

    )
    {
        $this->resource = $resource;
        $this->connection = $this->resource->getConnection();
    }


    /*
        Return Shipping Status By Order Id
    */

    public function getStatuses($id)
    {
        $status = '';
        $sql = "SELECT qty_ordered, qty_shipped  FROM sales_order_item WHERE order_id = $id;";
        $data = $this->connection->query($sql);
        $dataArray = $data->fetchAll();

        if (!empty($dataArray)) {
            $_ordered = [];
            $_shipped = [];


            foreach($dataArray as $item){
                $_ordered[] = intval($item['qty_ordered']);
                $_shipped[] = intval($item['qty_shipped']);
            }

            $ordered = array_sum($_ordered);
            $shipped = array_sum($_shipped);

            if ($shipped === 0) {
                $status = 0;
            }

            if ($shipped > 0 && $shipped < $ordered) {
                $status = 1;
            }

            if ($shipped === $ordered) {
                $status = 2;
            }
            return $status;

        }

    }
}