<?php

namespace DI\ShippingStatus\Model\Config;
use Magento\Framework\Data\OptionSourceInterface;

class Statuses implements OptionSourceInterface
{

    protected $options;
    /**
     * @return array
     */
    public function toOptionArray()
    {

        $options = [['value' => '', 'label' => __('-- Please Select --')]];

        $array = [' ', 'Partially Shipped', 'Fully Shipped'];

        foreach ($array as $code => $label) {
            $options[] = ['value' => $code, 'label' => $label];
        }

        $this->options = $options;

        return $this->options;
    }

}