<?php

namespace DI\Site\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

if (!function_exists("mb_str_replace")) {
    function mb_str_replace($needle, $replace_text, $haystack)
    {
        $haystack = preg_replace("/[^a-zA-ZА-Яа-я0-9,.—\s]/", "", $haystack);
        return implode($replace_text, mb_split($needle, $haystack));
    }
}

class Import extends Command
{
    protected  $_resource;
    public function __construct(
        \DI\Site\Excel\Reader $reader,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\State $state,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->reader = $reader;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->categoryCollection = $categoryCollection;
        $this->attribute = $attribute;
        $this->directoryList = $directoryList;
        $this->stockRegistry = $stockRegistry;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->state = $state;
        $this->_resource = $resource;

        parent::__construct();

    }

    protected function configure()
    {
        $this->setName('DI:prodimp');
        $this->setDescription('Import Products from file');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //die('test');
        //$this->state->setAreaCode('frontend');
        //$this->updateDubtAttributes();

//        $this->state->setAreaCode('frontend');
//        $this->updatePrice();
        die('stop');
        $excel = $this->reader;
        $excel->read('products.xls');

        $nrSheets = count($excel->sheets);

        for ($i = 0; $i < $nrSheets; $i++) {
//            $this->sheetData($excel->sheets[$i]);
            $this->addRelatedProducts($excel->sheets[$i]);
        }

        die('prodimp');
    }

    private function updateDubtAttributes()
    {
        //$this->state->setAreaCode('global');


        $collection = $this->_productCollectionFactory->create()
            ->addAttributeToSelect('description')
            ->addAttributeToSelect('benefits')
            ->addAttributeToSelect('usage');
        $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        foreach ($collection as $product){

                $entity_id = $product->getId();

                $description = $product->getDescription();

                if ($description != null) {
                    $resultString = $this->convert($description, 'description');
                    //die(var_dump($resultString));
                    $attribute_id = '75';
                    if($resultString != ''){
                    $saveAttr = $connection->query("UPDATE catalog_product_entity_text SET value = '$resultString' WHERE entity_id = '$entity_id' AND attribute_id = '$attribute_id'");
                    //die(var_dump(get_class_methods($saveAttr)));
                        $product->setData('description', $resultString);

                    }
                }




                $benefits = $product->getData('benefits');
                if ($benefits != null) {
                    $resultString = $this->convert($benefits, 'benefits');
                    //die(var_dump($resultString));

                    $attribute_id = '146';
                    if($resultString != '') {

                        $saveAttr = $connection->query("UPDATE catalog_product_entity_text SET value = '$resultString' WHERE entity_id = '$entity_id' AND attribute_id = '$attribute_id'");
                        $product->setBenefits($resultString);

                    }
                }

                $usage = $product->getData('usage');
                //die(var_dump($usage));

                if ($usage != null) {
                    $name = $connection->query("SELECT value FROM catalog_product_entity_varchar WHERE entity_id = '$entity_id' AND attribute_id = '73'")
                        ->fetchAll();
                    //die(var_dump(($name[0]['value'])));
                    $resultString = $this->convert($usage, 'usage', $name[0]['value']);
                    //die(var_dump($resultString));
                    $attribute_id = '162';

                    if($resultString != '') {

                        $saveAttr = $connection->query("UPDATE catalog_product_entity_text SET value = '$resultString' WHERE entity_id = '$entity_id' AND attribute_id = '$attribute_id'");
                        $product->setUsage($resultString);

                    }


                }

            }
    }

    private function convert($str, $type, $name ='')
    {
        if ($type == "description") {
            //die(var_dump(($str)));
            $firstSentence = substr($str, 0, (strpos($str, ".")));
            $result = substr($str, 0, strpos($str, $firstSentence, 1));
            return $result;
        } elseif ($type == "benefits") {
            $firstSentence = substr($str, 0, (strpos($str, ".")));
            //die(var_dump($str));
            if($firstSentence != '') {
                $sentences = substr($str, 0, strpos($str, $firstSentence, 1));
                $sentencesArr = explode("\"", $sentences);
                $resultArr = [];
                foreach ($sentencesArr as $sentance) {
                    if ($sentance === "") {
                        continue;
                    }
                    $resultArr[] = '<li>' . $sentance . '</li>';
                }
                return '<ul>' . implode("", $resultArr) . '</ul>';
            }
        } elseif ($type == "usage") {

            //die(var_dump($name));

            if(strpos($name , 'POUCH') !== false){

                return '<p>'.$str.'</p><p><img src="/pub/media/wysiwyg/Inhalation-Pouch-Usage-drawing-lrg.gif" alt="" /></p>';

            }elseif(strpos($name ,  'MIST') !== false){

                return '<p>'.$str.'</p><p><img src="/pub/media/wysiwyg/Mood-Mist-Usage-drawing-lrg.gif" alt="" /></p>';

            }elseif(strpos($name ,  'SCRUB') !== false){

                return '<p>'.$str.'</p><p><img src="/pub/media/wysiwyg/Sugar-Scrub-Usage-drawing-lrg.gif" alt="" /></p>';

            }elseif(strpos($name ,  'HAND') !== false){

                return '<p>'.$str.'</p><p><img src="/pub/media/wysiwyg/Hand-creme-Usage-drawing-lrg.gif" alt="" /></p>';

            }elseif(strpos($name ,  'LOTION') !== false){

                return '<p>'.$str.'</p><p><img src="/pub/media/wysiwyg/Body-Lotion-Usage-drawing-lrg.gif" alt="" /></p>';
            }
        }
    }

    private function updatePrice()
    {
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect();
        foreach ($collection as $product) {
            $product = $this->getProductBySku($product->getSku());
//            $product->setWebsiteId(1);
            $product->setAwSarpRegularPrice($product->getPrice());
            $product->save();
            dd([
                $product->getAwSarpRegularPrice(),
                $product->getId()
            ]);
        }
        die('END');
    }

    private function getRelatedSkus($row)
    {
        $cols = [
            '33',
            '34',
            '35',
            '36',
        ];

        $res = [];
        foreach ($cols as $col) {

            $alu = trim($row[$col]);

            $collection = $this->_productCollectionFactory->create()->addAttributeToFilter('alu', $alu);
            if (count($collection)) {
                $res[] = $collection->getFirstItem()->getSku();
            }
        }

        return $res;
    }

    private function addRelatedProducts($sheet)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();


        $x = 3;
        // rows
        while ($x <= $sheet['numRows']) {
            $tmpRow = $sheet['cells'][$x];
            $sku = trim($tmpRow[8]);
            if (!$sku) {
                break;
            }
            $product = $this->getProductBySku($sku);

            $relatedSkus = $this->getRelatedSkus($tmpRow);

            if (count($relatedSkus)) {
                $linkData = [];
                $pos = 1;
                foreach ($relatedSkus as $relatedSku) {
                    $productLink = $objectManager->create('Magento\Catalog\Api\Data\ProductLinkInterface')
                        ->setSku($product->getSku())
                        ->setLinkedProductSku($relatedSku)
                        ->setPosition($pos)
                        ->setLinkType('related');

                    $linkData[] = $productLink;
                    $pos++;
                }

                /**
                 * @var $product \Magento\Catalog\Model\Product\Interceptor
                 */
                $product = $objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface')
                    ->get($product->getSku());
                $product->setProductLinks($linkData)
                    ->save();
            }


            dd([
                'id' => $product->getId(),
                'step' => $x
            ]);

            $x++;
        }

        die('END');

        return true;

    }

    private function getProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }

    private function getOils($row)
    {
        $cols = [
            '11',
            '13',
            '15',
            '17',
        ];

        $res = [];
        foreach ($cols as $col) {
            $str = strpos($row[$col], ":");
            $label = substr($row[$col], 0, $str);

            $res[] = $this->getAttributeByCode('oils', $label);
        }

        return $res;
    }

    private function getIngredients($row)
    {
        $cols = [
            '11',
            '13',
            '15',
            '17',
            '19',
        ];

        $res = [];
        foreach ($cols as $col) {
            $str = strpos($row[$col], ":");

            if ($str !== false) {
                $label = substr($row[$col], 0, $str);
            } else {
                $label = trim($row[$col]);
            }

            $res[] = $this->getAttributeByCode('ingredients', $label);
        }

        return $res;
    }

    private function getWeigth($row)
    {
        if ($row{0} == '.') $row = '0' . $row;

        $row = str_replace('oz', '', $row);
        $row = str_replace(' ', '', $row);

        return $row;
    }

    private function getCategoryIds($row)
    {
        $names = explode(';', $row);

        $res = [];
        foreach ($names as $name) {
            $name = trim($name);

            $collection = $this->categoryCollection->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('name', ['like' => "%$name%"]);

            if (count($collection)) {
                foreach ($collection as $cat) {
                    $id = $cat->getId();
                    if (in_array($id, $res)) continue;
                    $res[] = $id;
                }
            }
        }

        return $res;
    }

    private function getProductForm($rows, $attrName)
    {
        $row = $rows[10];
        $names = explode(';', $row);

        $name = strtolower($names[0]);
        $name = str_replace('&', 'and', $name);
        $name = str_replace(' ', '_', $name);

        if ($name == $attrName) {
            return $this->getAttributeByCode($attrName, trim($rows[9]));
        }
        return '';
    }

    private function getAttributeByCode($code, $label)
    {
        $attr = $this->attribute->loadByCode('catalog_product', $code);

        foreach ($attr->getOptions() as $option) {
            $option = $option->toArray();
            if ($option['label'] == $label) {
                return $option['value'];
            }
        }
    }

    private function getImages($row)
    {
        $imagePath = 'import/images/';

        $mediaImportFolder = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $imageAbsolutePath = $mediaImportFolder . '/' . $imagePath;

        $cols = [
            '23',
            '24',
            '25',
            '26',
            '27',
        ];

        $res = [];
        foreach ($cols as $col) {
            if (!$row[$col]) continue;

            $file = $imageAbsolutePath . trim($row[$col]);
            if (file_exists($file)) {
                $res[] = $file;
            }
        }

        return $res;
    }

    private function getEmptyProductData()
    {
        return [
            'name' => '',
            'sku' => '',
            'price' => '',
            'attribute_set_id' => '4',
            'size' => '',
            'visibility' => '4',
            'description' => '',
            'short_description' => '',
            'type_id' => \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE,
            'status' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED,
            'mood' => '',
            'oils' => [],
            'ingredients' => [],
            'ingredients' => [],
            'category_ids' => [],
            'product_type' => '',
            'benefits' => '',
            'weight' => '',
            'bath_and_body' => '',
            'aromatherapy' => '',
            'alu' => '',
            'dheight' => '',
            'dlength' => '',
            'dwidth' => '',
        ];
    }

    private function sheetData($sheet)
    {
        $this->state->setAreaCode('frontend');

        $x = 3;
        // rows
        while ($x <= $sheet['numRows']) {
            $tmpRow = $sheet['cells'][$x];

            $moodLabel = utf8_encode(trim($tmpRow[3]));

            $data = $this->getEmptyProductData();

            $data['name'] = mb_strtoupper(str_replace('  ', ' ', trim($tmpRow[4])));

            $data['sku'] = trim($tmpRow[8]);
            $data['alu'] = trim($tmpRow[5]);

            $url = strtolower($data['name']);
            $url = str_replace(' ', '-', $url);
            $url .= '-' . $data['alu'];

            $data['url_key'] = $url;

            $data['price'] = trim($tmpRow[7]);
            $data['mood'] = $this->getAttributeByCode('mood', $moodLabel);
            $data['benefits'] = $tmpRow[30];

            $description = trim($tmpRow[28]);
            $data['description'] = mb_str_replace($moodLabel, "<em>$moodLabel</em>", $description);

            $data['weight'] = $this->getWeigth($tmpRow[41]);

            $data['oils'] = $this->getOils($tmpRow);
            $data['ingredients'] = $this->getIngredients($tmpRow);
            $data['category_ids'] = $this->getCategoryIds($tmpRow[10]);
            $data['bath_and_body'] = $this->getProductForm($tmpRow, 'bath_and_body');
            $data['aromatherapy'] = $this->getProductForm($tmpRow, 'aromatherapy');


            $data['dheight'] = trim($tmpRow[39]);
            $data['dlength'] = trim($tmpRow[38]);
            $data['dwidth'] = trim($tmpRow[40]);

            $data['usage'] = trim($tmpRow[29]);
            $data['caution'] = trim($tmpRow[31]);
            $data['ingredients_tab'] = trim($tmpRow[32]);

            $data['size'] = $this->getAttributeByCode('size', trim($tmpRow[6]));

            $images = $this->getImages($tmpRow);

            $productId = $this->createProduct($data, $images);

            dd([
                'id' => $productId,
                'step' => $x
            ]);

            $x++;
        }

        die('END');

        return true;
    }

    private function createProduct($data, $images)
    {
        $collection = $this->_productCollectionFactory->create()->addAttributeToFilter('sku', $data['sku']);
        if (count($collection)) {
            return $collection->getFirstItem()->getId();
        }

        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        $product = $this->productFactory->create();
        try {
            $product->setData($data);

            $product = $this->productRepository->save($product);

            if (count($images)) {
                foreach ($images as $image) {
                    $mediaAttribute = null;
                    if (stripos($image, 'main') !== false) {
                        $mediaAttribute = ['image', 'small_image', 'thumbnail'];
                    }
                    dd($image);
                    $product->addImageToMediaGallery($image, $mediaAttribute, false, false);
                }
            }

            $product->save();
        } catch (Exception $e) {
            d($e->getMessage());
        }

        return $product->getId();
    }
}