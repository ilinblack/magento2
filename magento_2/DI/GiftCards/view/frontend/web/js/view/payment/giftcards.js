/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Customer/js/model/customer',
        'DI_GiftCards/js/action/set-giftcards',
        'DI_GiftCards/js/action/cancel-giftcards',
        'Magento_Checkout/js/model/totals'
    ],
    function ($, ko, Component, quote, priceUtils, customer, setGiftCardsAction, cancelGiftCardsAction, modelTotals) {
        'use strict';
        var totals = quote.getTotals();
        var giftcardsValue = ko.observable(null);
        var isApplied = ko.observable(null);
        var isLoading = ko.observable(false);

        quote.totals.subscribe(function (total) {
            totals = total;

            if (modelTotals.getSegment('giftcards')) {
                giftcardsValue(modelTotals.getSegment('giftcards').value.toFixed(2));
                isApplied(true);
            } else {
                var giftCardBal = Number(window.customer_gift_card_value);
                if (giftCardBal > 0) {
                    if (typeof giftCardBal === "string") {
                        giftCardBal = parseInt(giftCardBal);
                    }
                    if (totals['base_grand_total'] > giftCardBal) {
                        giftcardsValue(giftCardBal.toFixed(2));
                    } else {
                        giftcardsValue((totals['base_grand_total']).toFixed(2));
                    }
                }
            }
        });

        return Component.extend({
            defaults: {
                template: 'DI_GiftCards/payment/giftcards'
            },
            getGiftCardsLimit: function () {
                if (typeof(window.customer_gift_card_value) !== "undefined") {
                    return Number(window.customer_gift_card_value);
                }
                return 0;
            },
            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },
            giftcardsValue: giftcardsValue,
            /**
             * Applied flag
             */
            isApplied: isApplied,
            isLoading: isLoading,
            /**
             * Gift Cards application procedure
             */
            apply: function () {
                if (this.validate()) {
                    isLoading(true);
                    setGiftCardsAction(giftcardsValue(), isApplied, isLoading);
                }
            },
            /**
             * Cancel
             */
            cancel: function () {
                if (this.validate()) {
                    isLoading(true);
                    cancelGiftCardsAction(isApplied, isLoading);
                }
            },
            /**
             * Gift Cards form validation
             *
             * @returns {boolean}
             */
            validate: function () {
                var form = '#giftcards-form';
                return $(form).validation() && $(form).validation('isValid');
            }
        });
    }
);
