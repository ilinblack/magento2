<?php

namespace DI\SDS\Block\Adminhtml\Grid\Edit;

/**
 * Adminhtml Add New Row Form.
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $request;
    protected $_helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        // \Magento\Framework\Data\Form $form,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Backend\Helper\Data $mageHelper,


        array $data = []
    )
    {
        $this->_helper = $mageHelper;
        $this->request = $request;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $action = $this->_helper->getUrl('*/*/upload', array('id' => $this->request->getParam('id')));

            $form = $this->_formFactory->create(

            ['data' => [
                'id' => 'upload_form',
                    'enctype' => 'multipart/form-data',
                        'action' => $action,
                        'method' => 'post'
                    ]
                            ]
                        );


            $this->setForm($form);
         $fieldset = $form->addFieldset('mr_sds_fieldset',
                array('legend'=>'File Upload'));

            $fieldset->addField('mr_sds_file', 'file', array(
                'label'     => 'PDF File To Import',
                'required'  => false,
                'name'      => 'mr_sds_file',
                ));

         $fieldset->addField('mr_sds_submit', 'submit', array(
                'label'     => "",
                'required'  => false,
                'after_element_html' => '<small>Click to upload file</small>',
                'name'      => 'submit',
                'value'=>"Upload File"
                ));

        $form->setUseContainer(true);
                $this->setForm($form);


            //$field->setValues($model->getData());

            //$field->setUseContainer(true);

                return parent::_prepareForm();
    }
}


