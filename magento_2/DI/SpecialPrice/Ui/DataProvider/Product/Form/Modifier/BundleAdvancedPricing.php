<?php

namespace DI\SpecialPrice\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;

class BundleAdvancedPricing extends \Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\BundleAdvancedPricing
{

    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Modify meta for MSRP fields.
     *
     * @param array $meta
     * @return array
     */
    private function modifyMsrpMeta(array $meta)
    {
        return $meta;
    }


}