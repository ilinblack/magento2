<?php

namespace DI\AgeVerification\Model;

class AgeVerification{

    public function __construct(
        \DI\AgeVerification\Model\AgeFactory $ageFactory
    )

    {
        $this->_ageFactory = $ageFactory;
    }


    public function getAgeVerificationByCustomerId($customerId)
    {
        $_collection = $this->_ageFactory->create();

        $collection = $_collection->getCollection()->addFilter('customer_id', $customerId)->load();

        return $collection;
    }


}