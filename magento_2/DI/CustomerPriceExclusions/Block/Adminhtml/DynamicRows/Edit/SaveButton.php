<?php

namespace DI\CustomerPriceExclusions\Block\Adminhtml\DynamicRows\Edit;



use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;



class SaveButton implements ButtonProviderInterface

{

    public function getButtonData()

    {

        $url = $this->getUrl('exclusions/row/save');

        return [

            'label' => __('Save Rows'),

            'class' => 'save primary',

            'on_click' => "setLocation('". $url ."'')",

            'sort_order' => 90,

        ];

    }

}