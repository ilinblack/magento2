<?php

namespace DI\AgeVerification\Block\Adminhtml\Order;

class View extends \Magento\Sales\Block\Adminhtml\Order\View{

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\Config $salesConfig,
        \Magento\Sales\Helper\Reorder $reorderHelper,
        \DI\AgeVerification\Model\AgeVerification $ageVerification,
        array $data = []
    )
    {
        $this->_reorderHelper = $reorderHelper;
        $this->_coreRegistry = $registry;
        $this->_salesConfig = $salesConfig;
        $this->ageVerification = $ageVerification;
        parent::__construct($context,$registry,$salesConfig,$reorderHelper,$data);
    }

    public function getAgeVerification($customerId)
    {
        return $this->ageVerification->getAgeVerificationByCustomerId($customerId);
    }

    public function isCustomerPassedVerification($customerId)
    {
        $_ageVerification = $this->getAgeVerification($customerId);
        return $_ageVerification->getLastItem()->getIsAgeVerificationPassed();
    }

    public function hasOrderRestrictedProducts()
    {
        $orderItems = $this->getOrder()->getItems();

        $isRestricted = false;

        foreach ($orderItems as $item){

            if($item->getProduct()->getData('is_age_restricted') != "0"){
                $isRestricted = true;
            }
        }

        return $isRestricted;

    }



}