<?php

namespace DI\GiftCards\Model\Total\Quote;

class Giftcard extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    protected $priceCurrency;

    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    )
    {
        $this->setCode('giftcards');
        $this->priceCurrency = $priceCurrency;
    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);

        $giftCardValue = $quote->getData('gift_card_value');
        if (!$giftCardValue) return $this;

        $allTotalAmounts = $total->getAllTotalAmounts();
        if ($allTotalAmounts['giftcards'] == 0 && $allTotalAmounts['subtotal'] > 0) {

            $newTotal = $allTotalAmounts['subtotal'] - abs($allTotalAmounts['discount']) + $allTotalAmounts['tax'] + $allTotalAmounts['shipping'];
            $balance = $newTotal - $giftCardValue;

            if ($giftCardValue > $newTotal || ($balance < 0.01 && $balance > 0)) {
                $giftCardValue = $newTotal;
                $quote->setData('gift_card_value', $giftCardValue);
            }

            $giftCard = $this->priceCurrency->convert($giftCardValue);
            $total->addTotalAmount($this->getCode(), -$giftCard);
            $total->addBaseTotalAmount($this->getCode(), -$giftCardValue);
        }
        return $this;
    }

    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $result = null;
        $amount = $quote->getData('gift_card_value');

        if ($amount != 0) {
            $result = [
                'code' => $this->getCode(),
                'title' => 'Gift card',
                'value' => $amount
            ];
        }
        return $result;
    }
}