<?php

namespace DI\GiftCards\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class SalesOrderInvoicePay implements ObserverInterface
{
    protected $giftCardHelper;
    protected $giftCardModel;

    public function __construct(
        \DI\GiftCards\Helper\Data $giftCardHelper,
        \DI\GiftCards\Model\GiftCards $giftCardModel
    )
    {
        $this->giftCardHelper = $giftCardHelper;
        $this->giftCardModel = $giftCardModel;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent()->getInvoice();
        $order = $event->getOrder();

        $orderGiftcardNumber = $order->getGiftCardNumber();
        $orderGiftcardValue = $order->getGiftCardValue();

        if ($orderGiftcardValue && $orderGiftcardValue > 0) {
            /**
             * Set gift card value to invoice
             */
            $event->setData('gift_card_value', $orderGiftcardValue);
            $customerGiftcardBalance = $this->giftCardHelper->getGiftCardBalance($orderGiftcardNumber);

            if ($customerGiftcardBalance && $customerGiftcardBalance > 0) {
                /**
                 * New balance of the customer's gift card
                 */
                $newCustomerGiftcardBalance = $customerGiftcardBalance - $orderGiftcardValue;

                if ($newCustomerGiftcardBalance < 0) {
                    throw new \Exception(__('Gift Card balance exhausted.'));
                }

                if ($customerId = $order->getCustomerId()) {
                    $customer = $this->giftCardHelper->getCustomer($customerId);
                    $this->giftCardHelper->setGiftcardValueToCustomer($customer, $newCustomerGiftcardBalance);
                }

                $sendData = [
                    "Value" => $orderGiftcardValue,
                    "OrderID" => $order->getIncrementId(),
                    "Platform" => "Magento"
                ];

                \Magento\Framework\App\ObjectManager::getInstance()
                    ->get(\Psr\Log\LoggerInterface::class)->debug("GiftCardSendData", [$orderGiftcardNumber, $sendData]);

                $this->giftCardModel->redeemGiftCard($orderGiftcardNumber, $sendData);
            }
        }
        return $this;
    }
}


