<?php
namespace DI\CustomersImport\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallData implements InstallDataInterface
{

    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /* @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $attributesInfo = [

            'gsap' =>[
            'label' => 'GSAP Ship To',
            'type' => 'varchar',
            'required' => true,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'user_defined' => true,
            'visible_on_front' => true,
            'system' => 0,
            'is_used_in_grid' => true
            ],
            'is_defined' => [
                'type' => 'int',
                'label' => 'Is Customer Defined',
                'input' => 'boolean',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'system' => 0,
                'is_used_in_grid' => true
            ],
            'service_station_name' => [
                'label' => 'Service Station Name',
                'required' => false,
                'type' => 'varchar',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'visible_on_front' => true,
                'system' => 0,
                'is_used_in_grid' => true
            ],
            'service_station_phone' => [
                'label' => 'Service Station Phone',
                'required' => false,
                'type' => 'varchar',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'visible_on_front' => true,
                'system' => 0,
                'is_used_in_grid' => true
            ]

        ];

        $attributesAddressInfo =[

            'district' => [
                'label' => 'District',
                'required' => false,
                'type' => 'static',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'visible_on_front' => true,
                'system' => 0,
                'is_used_in_grid' => true
            ]

        ];


        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /* @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        foreach ($attributesInfo as $attributeCode => $attributeParams) {
            $customerSetup->addAttribute(Customer::ENTITY, $attributeCode, $attributeParams);

            $magentoUsernameAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $attributeCode);
            $magentoUsernameAttribute->addData([
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer', 'customer_account_edit', 'customer_account_create'],
            ]);
                $magentoUsernameAttribute->save();
            }

        foreach ($attributesAddressInfo as $attributeCode => $attributeParams) {
            $customerSetup->addAttribute('customer_address', $attributeCode, $attributeParams);

            $magentoUsernameAttribute = $customerSetup->getEavConfig()->getAttribute('customer_address', $attributeCode);
            $magentoUsernameAttribute->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address','customer_address'],
            ]);
            $magentoUsernameAttribute->save();
        }

        $setup->endSetup();
    }

}