<?php
namespace DI\ProductDocs\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use \Magento\Catalog\Model\ResourceModel\Eav\Attribute;

/**
 * Data provider for "Datasheet" field of product page
 */
class File extends AbstractModifier
{
    /**
     * @param LocatorInterface            $locator
     * @param UrlInterface                $urlBuilder
     * @param ArrayManager                $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager,
        Attribute $attributeFactory
    ) {
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->_attributeFactory = $attributeFactory;
    }

    public function modifyMeta(array $meta)
    {
        $fileAttributes = $this->_attributeFactory->getCollection()->addFieldToFilter('frontend_input', 'file');

        if (count($fileAttributes)) {
            foreach($fileAttributes as $attribute)
            {
                $fieldCode = $attribute->getAttributeCode();
                $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');
                $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

                if (!$elementPath) {
                    return $meta;
                }

                $meta = $this->arrayManager->merge(
                    $containerPath,
                    $meta,
                    [
                        'children'  => [
                            $fieldCode => [
                                'arguments' => [
                                    'data' => [
                                        'config' => [
                                            'elementTmpl' => 'DI_ProductDocs/grid/filters/elements/file',
                                        ],
                                    ],
                                ],
                            ]
                        ]
                    ]
                );
            }
        }

        return $meta;
    }
    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }
}