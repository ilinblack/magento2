<?php

namespace DI\GiftCards\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $customerSession;
    protected $customerFactory;
    protected $giftCardModel;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \DI\GiftCards\Model\GiftCards $giftCardModel,
        \Magento\Backend\Model\Session\Quote $backendQuoteSession,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Store\Model\StoreManagerInterface $storeManager

    )
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->giftCardModel = $giftCardModel;
        $this->_backendQuoteSession = $backendQuoteSession;
        $this->_customer = $customer;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public static function mysqlEscapeMimic($inp)
    {
        if (is_array($inp))
            return array_map(__METHOD__, $inp);

        if (!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }

    public function getCustomer($customerId = null)
    {
        if ($customerId) {
            if (is_numeric($customerId)) {
                return $this->customerFactory->create()->load($customerId);
            } else {
                $storeId = $this->_storeManager->getStore()->getId();
                return $this->customerFactory->create()->setWebsiteId($storeId)->loadByEmail($customerId);
            }

        }
        if ($this->customerSession->getCustomer()) {
            if ($customerSessionId = $this->customerSession->getCustomer()->getId()) {
                return $this->customerFactory->create()->load($customerSessionId);
            }
        }

        if($this->_backendQuoteSession){
           $customerId = $this->_backendQuoteSession->getData('customer_id');
           if($customerId){
               return $this->customerFactory->create()->load($customerId);
           }
        }

        return $this->customerSession;
    }

    public function getCurrentCustomerGiftCardBalance()
    {
        if ($number = $this->getCustomerGiftCardNumber()) {
            $giftCard = $this->setGiftcardToCustomer($number);
            if ($giftCardValue = $giftCard['gift_card_value']) {
                return $giftCardValue;
            }
        }
           
        return 0;
    }

    public function getCustomerGiftCardBalance($customerId = null)
    {
        $customer = $this->getCustomer($customerId);
        if ($customer) {
            if ($customer instanceof \Magento\Customer\Model\Session) {
                if ($giftcardData = $this->customerSession->getGiftCardData()) {

                    return $giftcardData['gift_card_value'];
                }
            } else {

                return $customer->getGiftCardValue();
            }
        }
        return 0;
    }

    public function getCustomerGiftCardNumber($customerId = null)
    {
        $customer = $this->getCustomer($customerId);
        if ($customer) {
            if ($customer instanceof \Magento\Customer\Model\Session) {
                if ($giftcardData = $this->customerSession->getGiftCardData()) {
                    return $giftcardData['gift_card_number'];
                }

            } else {
                return $customer->getGiftCardNumber();
            }
        }
        return null;
    }

    public function getGiftCardBalance($giftcardno)
    {
        $giftcard = $this->giftCardModel->getGiftCard($giftcardno);
        if ($giftcardvalue = $giftcard['Value']) {
            return $giftcardvalue;
        }
        return 0;
    }

    public function validGiftcard($giftcard, $customer)
    {
        if (!isset($giftcard['GiftCardNo']) || !$giftcard['GiftCardNo']) {
            throw new \Exception(__('Gift Card Code is not valid.'));
        }

        $customerEmail = $customer->getEmail();
        if ($email = trim($giftcard['email'])) {
            if (strtolower($customerEmail) !== strtolower($email)) {
                throw new \Exception(__('This gift card associated to another customer.'));
            }
        }
        return true;
    }

    public function setGiftcardToCustomer($giftcardno, \Magento\Customer\Model\Customer $customer = null)
    {
        if (!$customer) $customer = $this->getCustomer();

        $giftcard = $this->giftCardModel->getGiftCard($giftcardno);
        $this->validGiftcard($giftcard, $customer);

        $giftcardno = $giftcard['GiftCardNo'];
        $giftcardvalue = $giftcard['Value'];

        return $this->setGiftcardValueToCustomer($customer, $giftcardvalue, $giftcardno);
    }

    public function setGiftcardValueToCustomer($customer, $giftcardvalue, $giftcardno = null)
    {
        if ($customer instanceof \Magento\Customer\Model\Customer) {
            $customerData = $customer->getDataModel();
            if ($giftcardno) {
                $customerData->setCustomAttribute('gift_card_number', $giftcardno);
            }
            $customerData->setCustomAttribute('gift_card_value', $giftcardvalue);
            $customer->updateData($customerData);
            $customer->save();
        } elseif ($customer instanceof \Magento\Customer\Model\Session) {
            $this->customerSession->setGiftCardData(
                ['gift_card_number' => $giftcardno, 'gift_card_value' => $giftcardvalue]
            );

        }
        return ['gift_card_number' => $giftcardno, 'gift_card_value' => $giftcardvalue];
    }
}