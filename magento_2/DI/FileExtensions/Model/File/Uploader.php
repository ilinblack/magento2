<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace DI\FileExtensions\Model\File;

class Uploader extends \Magento\MediaStorage\Model\File\Uploader
{

    public function checkMimeType($validTypes=[])
    {

        if(count($validTypes) < 0){
            return false;
        }
        return true;

    }
}

