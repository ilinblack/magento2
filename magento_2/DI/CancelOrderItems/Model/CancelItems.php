<?php
namespace DI\CancelOrderItems\Model;
use DI\CancelOrderItems\Api\CancelItemsInterface;

class CancelItems implements CancelItemsInterface{

    public function __construct(
        \Magento\Sales\Model\Order $orderCollection,
        \Magento\Framework\App\ResourceConnection $resource,
        \DI\CancelEmails\Model\EmailSender $emailSender,
        \DI\ApiOrders\Helper\Data $mrHelper
    )
    {
        $this->orderCollection = $orderCollection;
        $this->_resource = $resource;
        $this->connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->emailSender = $emailSender;
        $this->mrHelper = $mrHelper;
    }

    public function execute(
         string $orderId,
         string $items
    )
    {
        try {
            $_items = json_decode($items, true);

            $order = $this->orderCollection->load($orderId);

            foreach ($_items as $item) {

                $orderItem = $order->getItemById($item['item_id']);

                $qtyCanceled = $item['qty'] + $orderItem['qty_canceled'];

                if ($orderItem['qty_ordered'] >= $qtyCanceled) {

                    $orderItem->setData('qty_canceled', $qtyCanceled);
                }
            }

            $_itemStatusOrdered = [];
            $_itemStatusCanceled = [];

            foreach ($order->getAllItems() as $item) {
                $_itemStatusOrdered[] = $item['qty_ordered'];
                $_itemStatusCanceled[] = $item['qty_canceled'];
            }

            $itemStatusOrdered = array_sum($_itemStatusOrdered);
            $itemStatusCanceled = array_sum($_itemStatusCanceled);

            if ($itemStatusOrdered == $itemStatusCanceled) {
                $order->setStatus('canceled');
            } else {
                $sendEmail = $this->emailSender->send($order);
                if(!$sendEmail){
                    throw new \Exception();
                }
            }


            $order->save();
 
            $job = $this->mrHelper->sendOrder($order);
            $job->send();

        }catch (\Exception $e){
        }
    }
}