<?php
namespace DI\CounterPoint\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{

    protected $_pricingHelper;

    public function __construct(
        \Magento\Framework\Pricing\Helper\Data $helper,
        \Magento\Catalog\Model\Product $productCollectionFactory

    )
    {
          $this->_pricingHelper = $helper;
          $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function calculateGrossProfit($cost, $price, $gp = null)
    {
        if($gp === 0){
            return 0;
        }elseif ($gp === 100){
            return 100;
        }

        if($cost && $price != null) {

            $gm = (($price - $cost) / $price) * 100;

            return number_format((float)$gm, 2, '.', '');
        }

    }

    public function calculatePriceOfGrossProfit($cost, $price, $gp = null)
    {
        if($price && $cost && $gp != null) {
            $denominator = (100 - $gp);
            if($denominator == 0) {
                // impossible to divide by zero
                $denominator = 1;
            }
                $result = (100 * $cost) / $denominator;
                return number_format((float)$result, 2, '.', '');
        }
    }
    public function calculatePriceOfCustomPrice($customprice, $custompriceOf)
    {
        if ($customprice && $custompriceOf != null) {

            $result_price = $customprice - ($customprice * ($custompriceOf / 100));

            return number_format((float)$result_price, 2, '.', '');
        }
    }
    public function calculateAvarageGP(array $gpArray){

        $count = count($gpArray);

        $gpSum = array_sum ($gpArray);

        $avarageGP = $gpSum/$count;

        return $avarageGP;

    }

    public function getBundleCost($product){

        $endcost = 0;

        $selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection(
            $product->getTypeInstance(true)->getOptionsIds($product), $product
        );

        foreach ($selectionCollection as $option) {
            $qty = $option->getSelectionQty();
            $_product = $this->_productCollectionFactory->load($option->getId());
            $endcost += $_product->getCost() * $qty;
        }

        return $endcost;
    }

    public function getBundlePrice($product){

        $endprice = 0;

        $selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection(
            $product->getTypeInstance(true)->getOptionsIds($product), $product
        );

        if (!count($selectionCollection)) {
            return $endprice;
        }

        foreach ($selectionCollection as $option) {
            $qty = $option->getSelectionQty();
            $_product = $this->_productCollectionFactory->load($option->getId());
            $endprice += $_product->getFinalPrice() * $qty;
        }

        return $endprice;

    }

}