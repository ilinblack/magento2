<?php

namespace DI\GiftCards\Block\Customer;

class GiftCards extends \Magento\Customer\Block\Account\Dashboard
{
    protected $_collection;
    protected $_collectionFactory;
    protected $currentCustomer;
    protected $giftCardsModel;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement,
        \Magento\Review\Model\ResourceModel\Review\Product\CollectionFactory $collectionFactory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \DI\GiftCards\Model\GiftCards $giftCardsModel,
        array $data = []
    )
    {
        $this->_collectionFactory = $collectionFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->currentCustomer = $currentCustomer;
        $this->giftCardsModel = $giftCardsModel;

        parent::__construct(
            $context,
            $customerSession,
            $subscriberFactory,
            $customerRepository,
            $customerAccountManagement,
            $data
        );
    }

    public function getApplyGiftCardAction()
    {
        return $this->getBaseUrl() . 'gift-cards/customer/apply';
    }
}
