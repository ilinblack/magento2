<?php

namespace DI\PriceExclusions\Api\Data;

interface ApiInterface
{

    const ID = 'entity_id';
    const CUSTOMER_ID = 'customer_id';
    const RECORD_ID = 'record_id';
    const CUSTOMER_GROUP_IDS = 'customer_group_ids';
    const BRAND_ID = 'brand_id';
    const FILE_NAME = 'file_name';

    public function getId();


    public function setId($Id);


    public function getCustomerId();


    public function setCustomerId($customerId);


    public function getRecordId();


    public function setRecordId($recordId);


    public function getCustomerGroupIds();


    public function setCustomerGroupIds($customerGroupIds);

    public function getBrandId();

    public function setBrandId($brandId);

}