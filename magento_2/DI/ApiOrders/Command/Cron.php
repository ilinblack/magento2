<?php

namespace DI\ApiOrders\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Cron extends Command
{

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\Order $orderModel,
        \DI\ApiOrders\Helper\Data $apiOrdersHelper,
        \DI\ApiOrders\Model\Queue $apiOrdersQueue,
        \Magento\Framework\App\State $state
    )
    {
        $this->storeManager = $storeManager;
        $this->orderModel = $orderModel;
        $this->apiOrdersHelper = $apiOrdersHelper;
        $this->queue = $apiOrdersQueue;
        $this->state = $state;
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND); // or \Magento\Framework\App\Area::AREA_ADMINHTML, depending on your needs
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('apiorders:cron');
        $this->setDescription('Send orders,invoices and creditmemos to (Cron emulation)');
        parent::configure();
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {



        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $objectManager->get('DI\ApiOrders\Cron\Export')->execute();





    }








}