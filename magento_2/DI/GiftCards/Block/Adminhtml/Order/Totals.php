<?php

namespace DI\GiftCards\Block\Adminhtml\Order;

class Totals extends \Magento\Sales\Block\Adminhtml\Order\Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();
        if (!$this->getSource()->getIsVirtual() && $this->getSource()->getGiftCardValue() != 0
        ) {
            $this->_totals['giftcards'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'giftcards',
                    'value' => '-' . $this->getSource()->getGiftCardValue(),
                    'base_value' => '-' . $this->getSource()->getGiftCardValue(),
                    'label' => __('Gift Card value (' . $this->getSource()->getGiftCardNumber() . ')'),
                ]
            );
        }
        $this->_totals['paid'] = new \Magento\Framework\DataObject(
            [
                'code' => 'paid',
                'strong' => true,
                'value' => $this->getSource()->getTotalPaid(),
                'base_value' => $this->getSource()->getBaseTotalPaid(),
                'label' => __('Total Paid'),
                'area' => 'footer',
            ]
        );
        $this->_totals['refunded'] = new \Magento\Framework\DataObject(
            [
                'code' => 'refunded',
                'strong' => true,
                'value' => $this->getSource()->getTotalRefunded(),
                'base_value' => $this->getSource()->getBaseTotalRefunded(),
                'label' => __('Total Refunded'),
                'area' => 'footer',
            ]
        );
        $this->_totals['due'] = new \Magento\Framework\DataObject(
            [
                'code' => 'due',
                'strong' => true,
                'value' => $this->getSource()->getTotalDue(),
                'base_value' => $this->getSource()->getBaseTotalDue(),
                'label' => __('Total Due'),
                'area' => 'footer',
            ]
        );
        return $this;
    }
}
