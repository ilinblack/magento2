<?php

namespace DI\GiftCards\Model;

use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Free;

class MethodList extends \Magento\Payment\Model\MethodList
{
    protected $paymentHelper;
    protected $methodSpecificationFactory;

    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Payment\Model\Checks\SpecificationFactory $specificationFactory
    )
    {
        $this->paymentHelper = $paymentHelper;
        $this->methodSpecificationFactory = $specificationFactory;
    }

    public function getAvailableMethods(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        $store = $quote ? $quote->getStoreId() : null;
        $methods = [];
        $isFreeAdded = false;
        $method_giftcards = null;
        foreach ($this->paymentHelper->getStoreMethods($store, $quote) as $method) {
            if ($this->_canUseMethod($method, $quote)) {
                $method->setInfoInstance($quote->getPayment());
                $methods[] = $method;
                if ($method->getCode() == Free::PAYMENT_METHOD_FREE_CODE) {
                    $isFreeAdded = true;
                }
            }

            if ($method->getCode() == 'giftcards') {
                $method_giftcards = $method;
            }
        }

        if (!$isFreeAdded) {
            $freeMethod = $this->paymentHelper->getMethodInstance(Free::PAYMENT_METHOD_FREE_CODE);
            if ($freeMethod->isAvailableInConfig()) {
                $freeMethod->setInfoInstance($quote->getPayment());
                $methods[] = $freeMethod;
                if (!$method_giftcards) {
                    $methods[] = $freeMethod;
                } else {
                    $methods[] = $method_giftcards;
                }
            }
        }
        return $methods;
    }

    protected function _canUseMethod($method, \Magento\Quote\Api\Data\CartInterface $quote)
    {
        return $this->methodSpecificationFactory->create(
            [
                AbstractMethod::CHECK_USE_FOR_COUNTRY,
                AbstractMethod::CHECK_USE_FOR_CURRENCY,
                AbstractMethod::CHECK_ORDER_TOTAL_MIN_MAX,
            ]
        )->isApplicable(
            $method,
            $quote
        );
    }
}
