<?php

namespace DI\ShippingStatus\Ui\Component\Listing\Columns;

use Magento\Framework\Escaper;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class Shipping extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        \DI\ShippingStatus\Model\Statuses $statuses

    )
    {
        $this->statuses = $statuses;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {

                $id = $item['entity_id'];

                $status = $this->statuses->getStatuses($id);
                $item[$fieldName] = $this->prepareHtml($status);
            }
        }

        return $dataSource;
    }

    public function prepareHtml($status){
        $html = '';

        $url = '/pub/media/DI/ship_status/icons';

        switch ($status) {
            case 0:
                $html = '<div class="not_active">'.'<img src="'.$url.'/fully.png" style="opacity:0.3">'.'</div>';
                break;
            case 1:
                $html = '<div class="partial">'.'<img src="'.$url.'/partial.png">'.'</div>';
                break;
            case 2:
                $html = '<div class="partial">'.'<img src="'.$url.'/fully.png">'.'</div>';
                break;
        }
        return $html;

    }

}