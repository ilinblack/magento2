<?php


namespace DI\SDS\Model;

use DI\SDS\Api\Data\GridInterface;

class Grid extends \Magento\Framework\Model\AbstractModel implements GridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'DI_sds';

    /**
     * @var string
     */
    protected $_cacheTag = 'DI_sds';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'DI_sds';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('DI\SDS\Model\ResourceModel\Grid');
    }
    /**
     * Get SDSId.
     *
     * @return int
     */
    public function getSDSId()
    {
        return $this->getData(self::SDS_ID);
    }

    /**
     * Set SDSId.
     */
    public function setSDSId($SDSId)
    {
        return $this->setData(self::SDS_ID, $SDSId);
    }

    /**
     * Get Category.
     *
     * @return varchar
     */
    public function getCategory()
    {
        return $this->getData(self::CATEGORY);
    }

    /**
     * Set Category.
     */
    public function setCategory($category)
    {
        return $this->setData(self::CATEGORY, $category);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get UpdatedAt.
     *
     * @return varchar
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get FilePath.
     *
     * @return varchar
     */
    public function getFilePath()
    {
        return $this->getData(self::FILE_PATH);
    }

    /**
     * Set FilePath.
     */
    public function setFilePath($file_path)
    {
        return $this->setData(self::FILE_PATH, $file_path);
    }
    /**
     * Get FileName.
     *
     * @return varchar
     */
    public function getFileName()
    {
        return $this->getData(self::FILE_NAME);
    }

    /**
     * Set FileName.
     */
    public function setFileName($file_name)
    {
        return $this->setData(self::FILE_NAME, $file_name);
    }


}