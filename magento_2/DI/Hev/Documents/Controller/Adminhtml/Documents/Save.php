<?php
namespace DI\Documents\Controller\Adminhtml\Documents;

use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var \DI\Documents\Model\Documents
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \DI\Documents\Model\Documents $model
     */
    public function __construct(
        Action\Context $context,
        \DI\Documents\Model\Documents $model
    ) {
        parent::__construct($context);
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('DI_Documents::docs_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            /** @var \DI\Documents\Model\Documents $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('doc_id');

            //If file upload
            if(isset($data['path'][0]['file'])){

                if(!$data['doc_name'] && $data['path']){
                    $data['doc_name'] = $data['path'][0]['name'];
                }

                $path = str_replace('.tmp','',$data['path'][0]['file']);

                $data['doc_size'] = $data['path'][0]['size'];
                $data['file_name'] = $data['path'][0]['name'];

                if(!$id) {
                    $data['path'] = $path;
                }

            }

            if ($id) {
                $model->load($id);

                if(!isset($data['path'][0]['file'])){
                    unset($data['path']);
                }else{
                    $data['path'] = $path;
                }

            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'documents_prepare_save',
                ['documents' => $model, 'request' => $this->getRequest()]
            );

            try {
                $model->save();
                $this->messageManager->addSuccess(__('Document saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['doc_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the document'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}