<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace DI\CounterPoint\Block\Adminhtml\Order\Create;

use Magento\Quote\Model\Quote\Item;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Session\SessionManagerInterface;



/**
 * Adminhtml sales order create items block
 *
 * @api
 * @author     Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class Items extends \Magento\Sales\Block\Adminhtml\Order\Create\Items\Grid
{


    protected $locationBasedInventory;
    public $stockRegistry;

    public $countProductsInLocations = [];

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \DI\LocationBasedInventory\Model\Locations $locationBasedInventory,
        \Magento\Catalog\Model\ProductFactory $_productloader,

        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Sales\Model\AdminOrder\Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
        \Magento\GiftMessage\Model\Save $giftMessageSave,
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\GiftMessage\Helper\Message $messageHelper,
        StockRegistryInterface $stockRegistry,
        StockStateInterface $stockState,
        array $data = []

    ) {
        $this->locationBasedInventory = $locationBasedInventory;
        $this->_productloader = $_productloader;
        $this->_sessionQuote = $sessionQuote;
        $this->_messageHelper = $messageHelper;
        $this->_wishlistFactory = $wishlistFactory;
        $this->_giftMessageSave = $giftMessageSave;
        $this->_taxConfig = $taxConfig;
        $this->_taxData = $taxData;
        $this->stockRegistry = $stockRegistry;
        $this->stockState = $stockState;
        parent::__construct($context, $sessionQuote, $orderCreate, $priceCurrency,$wishlistFactory,$giftMessageSave,$taxConfig, $taxData, $messageHelper, $stockRegistry, $stockState, $data);
    }


    private function getBaseLocationsList($baseLocations)
    {
        $itemLocations = [];
        foreach ($baseLocations as $baseLocation) {
            $baseLocation['qty'] = 0;
            $baseLocation['backorder'] = false;
            $itemLocations[$baseLocation['id']] = $baseLocation;

            $this->countProductsInLocations[$baseLocation['id']] = 0;
        }
        return $itemLocations;
    }


    public function getLocations()
    {

        $list = [];
        /**
         * Get Locations List
         */
        $locationsList = $this->locationBasedInventory->getApiListLocations();
        $baseLocationsList = $this->getBaseLocationsList($locationsList);

        $skusList = [];
//        $orderItems = $this->getOrder()->getAllVisibleItems();
        $orderItems = $this->_sessionQuote->getQuote()->getAllItems();

        foreach ($orderItems as $item) {
            if (in_array($item->getProductType(), ['bundle', 'configurable'])) {
                continue;
            }
            $sku = $item->getSku();
            $list[$sku] = $baseLocationsList;
            $skusList[] = $sku;
        }

        /**
         * Get Products Stocks and Locations List
         */
        $stocks = $this->locationBasedInventory->getApiStocksAndListLocations($skusList);

        if ($stocks) {
            foreach ($stocks as $stock) {
                $sku = $stock['id'];
                $stockLocationId = $stock["location"]['id'];

                $this->countProductsInLocations[$stockLocationId] += 1;

                if (!isset($list[$sku]) || !isset($list[$sku][$stockLocationId])) {
                    continue;
                }

                if ($this->stockRegistry->getStockItemBySku($sku)->getBackorders()) {
                    $list[$sku][$stockLocationId]['backorder'] = true;
                }

                $list[$sku][$stockLocationId]['qty'] = $stock['qty'];
            }
        }

        return $list;

    }

    public function getProductById($id){
        return $this->_productloader->create()->load($id);
    }


}
