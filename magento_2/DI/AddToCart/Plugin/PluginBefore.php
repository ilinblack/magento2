<?php
namespace DI\AddToCart\Plugin;

use Magento\Checkout\Model\Cart as Subject;
use Magento\Catalog\Model\ProductFactory;

class PluginBefore
{
    protected $_productFactory;


    public function __construct(
        ProductFactory $productFactory
    ) {
        $this->_productFactory = $productFactory;
    }

    public function getBundleProductOptionsData($productId)
    {

        $product = $this->_productFactory->create()->load($productId);


        $selectionCollection = $product->getTypeInstance(true)
            ->getSelectionsCollection(
                $product->getTypeInstance(true)->getOptionsIds($product),
                $product
            );

        foreach ($selectionCollection as $proselection) {
            $params['bundle_option'][$proselection->getOptionId()] = $proselection->getSelectionId();
        }


            return $params['bundle_option'];
        }


    public function beforeAddProduct(Subject $subject,$productInfo,$requestInfo)
    {

        if($productInfo->getTypeId() === 'bundle'){
            $options = $this->getBundleProductOptionsData($productInfo->getId());
            $requestInfo =array_merge($requestInfo, array('bundle_option'=>$options));
        }

        return array($productInfo,$requestInfo);
    }
}