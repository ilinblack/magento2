<?php

namespace DI\ProductDocs\Model\Attribute\Backend;

class File extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{

    protected $_uploaderFactory;

    protected $_filesystem;

    protected $_fileUploaderFactory;

    protected $_logger;

    protected $_allowedExtensions = [];

    protected $_mediaPath = 'file';

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\File\UploaderFactory $uploaderFactory
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        $this->_uploaderFactory = $uploaderFactory;
    }

    public function setMediaPath($path = '') {
        $this->_mediaPath = $path;
    }

    public function afterSave($object)
    {
        $attributeName = $this->getAttribute()->getName();

        $isDelete = $object->getData($attributeName.'_delete');
        if ($isDelete === 'on') {
            $object->setData($attributeName, '');
            $this->getAttribute()->getEntity()->saveAttribute($object, $attributeName);
            return $this;
        }

        if (!$_FILES || !isset($_FILES['product']['name'][$attributeName]) || !$_FILES['product']['name'][$attributeName]) {
            return $this;
        }

        $fileName = $this->uploadFileAndGetName($attributeName, $this->_filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath($this->_mediaPath));
        if ($fileName) {
            $object->setData($attributeName, $fileName);
            $this->getAttribute()->getEntity()->saveAttribute($object, $attributeName);
        }

        return $this;
    }

    public function uploadFileAndGetName($input, $destinationFolder)
    {
        try {
            $uploader = $this->_uploaderFactory->create(array('fileId' => 'product['.$input.']'));

            if ($this->_allowedExtensions) {
                $uploader->setAllowedExtensions($this->_allowedExtensions);
            }

            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $uploader->setAllowCreateFolders(true);
            $uploader->save($destinationFolder);

            return $uploader->getUploadedFileName();
        } catch (\Exception $e) {
    }

        return '';
    }
}
