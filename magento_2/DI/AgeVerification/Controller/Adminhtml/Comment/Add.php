<?php

namespace DI\AgeVerification\Controller\Adminhtml\Comment;

class Add extends \Magento\Backend\App\Action
{

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Backend\Model\Auth\Session $authSession,
        \DI\AgeVerification\Model\AgeFactory $customerAge

    )
    {
        $this->customerAge = $customerAge;
        $this->_messageManager = $messageManager;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->authSession = $authSession;

        parent::__construct($context);
    }

    public function execute()
    {

        $result = $this->resultJsonFactory->create();

        $request = $this->getRequest();

        if($request->isAjax()) {

            $data = $request->getPost();

            if(array_key_exists('verification' ,$data)){
                $data['verification'] = 1;
            }else{
                $data['verification'] = 0;
            }


            $collection = $this->customerAge->create();

            $setComment = $this->setNewComment($data, $collection);

            if($setComment){
                $data['created_at'] = date('Y-m-d h:i:s');
                $user = $this->authSession->getUser();

                $data['user_name'] = $user->getUserName();
                $data['full_name'] = $user->getFirstName()." ". $user->getLastName();

                $result->setData($data);
            }else{
                 $this->_messageManager->addError('Something went wrong. Please, try later.');
            }

            return $result;

        }

    }

    public function setNewComment($data, $collection){

        try {
            if ($data && $collection) {
                $collection->setComment($data['comment']);
                $collection->setCustomerId($data['customer_id']);
                $collection->setUserId($data['user_id']);
                $collection->setIsAgeVerificationPassed($data['verification']);
                $collection->save();
            }
        }catch (\Exception $e){
            return false;
        }
        return true;
    }
}
