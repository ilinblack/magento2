<?php

namespace DI\SearchBreadcrumbs\Model\Plugin;

class Result
{

    public function __construct
    (
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        $this->resource = $resource;
    }

    public function afterToArray(\Mirasvit\SearchAutocomplete\Model\Result $subject, $result)
    {

        $storeId = $this->_storeManager->getStore()->getId();

            try {


                $connection = $this->resource->getConnection();

                /**
                 * Find All Product Ids
                 */
                $productSkus = [];
                foreach ($result['indices'] as $index) {
                        foreach ($index['items'] as $item) {
                            $productSkus[] = $item['sku'];
                        }
                }

                /**
                 * Find All Longest Urls
                 */
                $productUrls = [];

                $skus = implode(',', $productSkus);
                if ($skus && $connection) {
                    $baseUrl = $this->_storeManager->getStore()->getBaseUrl();

                    $sql = "SELECT ur.*, cpe.sku FROM `url_rewrite` as ur
                            LEFT JOIN `catalog_product_entity` as cpe ON cpe.entity_id = ur.entity_id
                            WHERE ur.entity_type LIKE '%product%' AND ur.store_id = '2' AND cpe.sku IN ($skus)";

                    $urls = $connection->fetchAll($sql);

                    foreach ($urls as $url) {
                        $sku = $url['sku'];
                        $requestPath = $baseUrl . $url['request_path'];
                        if (!isset($productUrls[$sku])) {
                            $productUrls[$sku] = $requestPath;
                        } elseif ((substr_count($productUrls[$sku], '/')) < (substr_count($requestPath, '/'))) {
                            $productUrls[$sku] = $requestPath;
                        }
                    }
                }

                /**
                 * Update Products Urls
                 */
                foreach ($result['indices'] as $key => $index) {


                        foreach ($index['items'] as $itemKey => $item) {

                            if (!isset($productUrls[$item['sku']])) continue;
                            $result['indices'][$key]['items'][$itemKey]['url'] = $productUrls[$item['sku']];
                        }
                }

            } catch (\Exception $ex) {
                // please check your exception log
            }
            return $result;

    }

}