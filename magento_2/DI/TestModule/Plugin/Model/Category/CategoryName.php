<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace DI\TestModule\Plugin\Model\Category;

use Magento\Catalog\Model\Category;

class CategoryName
{
    /**
     * @param Category $subject
     * @param  string $result
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */

    public function afterGetName(Category $subject, $result)
    {

        return 'Hello World ' . $result;

    }

}
