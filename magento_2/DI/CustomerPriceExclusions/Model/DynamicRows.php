<?php

namespace DI\CustomerPriceExclusions\Model;

use Magento\Framework\Model\AbstractModel;



class DynamicRows extends AbstractModel

{

    const CACHE_TAG = 'customer_price_exclusions';


    protected $_cacheTag = 'customer_price_exclusions';


    protected $_eventPrefix = 'customer_price_exclusions';


    protected function _construct()

    {
        $this->_init('DI\CustomerPriceExclusions\Model\ResourceModel\DynamicRows');

    }



}