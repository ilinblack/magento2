<?php
namespace DI\SDS\Block;


class Display extends \Magento\Framework\View\Element\Template
{
    protected $gridFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \DI\SDS\Model\GridFactory $gridFactory
    )
    {
        parent::__construct($context);

        $this->gridFactory = $gridFactory;
    }

    public function getSDS()
    {
        $data = $this->gridFactory->create();

        $collection = $data->getCollection()->getItems();


        $sds = [];

        foreach ($collection as $item){

                 $sds[$item->getCategory()][] = ['id' => $item->getSDSId(),
                     'category' => $item->getCategory(),
                     'name' => $item->getFileName(),
                     'file_path' => $item->getFilePath()];
        }

        return $sds;
    }
}