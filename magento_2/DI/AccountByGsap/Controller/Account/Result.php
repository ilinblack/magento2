<?php
namespace DI\AccountByGsap\Controller\Account;

class Result extends \Magento\Framework\App\Action\Action{

    protected $_customerFactory;
    protected $_addressFactory;
    protected $_resultJsonFactory;
    protected $_messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\AddressFactory $addressFactory
)
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }
    public function execute(){

       $data = $this->getRequest()->getPost();

        $customerCollection = $this->_customerFactory->create()
                ->addAttributeToSelect("*")
                ->load();
        $result = $this->_resultJsonFactory->create();

        $resultData= [];
        $gsapArray = [];

        foreach ($customerCollection as $customer){

            $gsap = $customer->getAttribute('gsap')->getFrontend()->getValue($customer);

            $gsapArray[] = $gsap;

            if($gsap === trim($data['gsap'])){

                $ssn = $customer->getAttribute('service_station_name')->getFrontend()->getValue($customer);
                $ssph = $customer->getAttribute('service_station_phone')->getFrontend()->getValue($customer);

                $customerId = $customer->getId();


                if($customer->getIsDefined()){
                    $this->_messageManager->addError('You are already finished your verification. Please, login with your email and password.');
                    $resultData['fail'] = 1 ;
                    return $result->setData($resultData);
                }

            $addresses = $this->_addressFactory->create()->getCollection()->setCustomerFilter($customer)->load()->getData();
            if(empty($addresses)){
                $resultData['success'] = [
                    'gsap'=>$gsap,
                    'ssn'=>$ssn,
                    'ssph'=>$ssph,
                    'customerId'=>$customerId,
                    ];
            }
            foreach ($addresses as $address){

            $resultData['success'] = [
                'gsap'=>$gsap,
                'ssn'=>$ssn,
                'ssph'=>$ssph,
                'street'=>$address['street'],
                'district'=>$address['district'],
                'city'=>$address['city'],
                'region'=>$address['region'],
                'postcode'=>$address['postcode'],
                'telephone'=>$address['telephone'],
                'customerId'=>$customerId,
            ];
            }
                $this->_messageManager->addSuccess('Please, confirm your personal data.');
                return $result->setData($resultData);
            }
        }
        if(!in_array(trim($data['gsap']), $gsapArray)){
            $this->_messageManager->addError('Sorry, wrong GSAP code. Try one more time.');
        }

    }

}