<?php

namespace DI\GiftCards\Model;

class Payment extends \Magento\Payment\Model\Method\AbstractMethod
{
    const CODE = 'giftcards';
    protected $_code = self::CODE;
    protected $_isOffline = true;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \DI\GiftCards\Helper\Data $giftCardHelper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\State $state

    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );
        $this->giftCardHelper = $giftCardHelper;
        $this->cart = $cart;
        $this->_state = $state;
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        $quote = $this->cart->getQuote();

        $giftcardBalance = $this->giftCardHelper->getCustomerGiftCardBalance();

        if (!$giftcardBalance || $giftcardBalance <= 0) {
            return false;
        }

        if (
            $quote &&
            (abs($quote->getGrandTotal()) == 0 || number_format($quote->getData('grand_total'), 4, '.', ' ') == 0) &&
            abs($quote->getGiftCardValue()) > 0
        ) {
            return true;
        }

        if(!$this->isAdminArea()) {
            if ($quote && abs($quote->getGrandTotal()) != abs($quote->getGiftCardValue())) {
                return false;
            }
        }

        if ($quote && ($quote->getBaseGrandTotal() > $giftcardBalance)) {
            return true;
        }


        return parent::isAvailable($quote);
    }
    private function isAdminArea()
    {
        return 'adminhtml' === $this->_state->getAreaCode();
    }

}