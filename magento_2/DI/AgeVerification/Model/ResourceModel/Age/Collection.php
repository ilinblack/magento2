<?php
namespace DI\AgeVerification\Model\ResourceModel\Age;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'customer_age_verification';
    protected $_eventObject = 'customer_age_verification';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('DI\AgeVerification\Model\Age', 'DI\AgeVerification\Model\ResourceModel\Age');
    }

}
