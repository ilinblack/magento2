<?php

namespace DI\GiftCards\Model;

use DI\GiftCards\Api\GiftcardInterface;

class Card implements GiftcardInterface
{
    protected $quoteRepository;
    protected $cart;

    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Checkout\Model\Cart $cart,
        \DI\GiftCards\Helper\Data $giftCardHelper
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->cart = $cart;
        $this->giftCardHelper = $giftCardHelper;
    }

    public function set($giftCardValue)
    {
        $quote = $this->cart->getQuote();
        if (!$quote->getItemsCount()) {
            throw new \Exception(__('Cart %1 doesn\'t contain products', $quote->getId()));
        }

        if ($giftCardValue > $quote->getGrandTotal()) {
            $giftCardValue = $quote->getGrandTotal();
        }

        $giftcardBalance = $this->giftCardHelper->getCurrentCustomerGiftCardBalance();
        if ($giftcardBalance) {
            if ($giftcardBalance < $giftCardValue) {
                throw new \Exception(__("Your Gift Card balance $giftcardBalance"));
            }
        } else {
            throw new \Exception(__('Your do not have Gift Card balance.'));
        }

        try {
            $quote->setData('gift_card_value', $giftCardValue);
            $this->quoteRepository->save($quote->collectTotals());
        } catch (\Exception $e) {
            throw new \Exception(__('Could not apply gift card'));
        }
        return true;
    }

    public function cancel()
    {
        $quote = $this->cart->getQuote();
        try {
            $quote->setData('gift_card_value', '0');
            $this->quoteRepository->save($quote->collectTotals());
        } catch (\Exception $e) {
            throw new \Exception(__('Could not remove gift card'));
        }
        return true;
    }
}