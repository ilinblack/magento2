<?php

namespace DI\GiftCards\Block\Adminhtml\Order\Create\Form;

use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Account extends \Magento\Sales\Block\Adminhtml\Order\Create\Form\Account{

    public $_sessionQuote;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Sales\Model\AdminOrder\Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Customer\Model\Metadata\FormFactory $metadataFormFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        array $data = []
    ) {
        $this->_sessionQuote = $sessionQuote;
        $this->_metadataFormFactory = $metadataFormFactory;
        $this->customerRepository = $customerRepository;
        $this->_extensibleDataObjectConverter = $extensibleDataObjectConverter;
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $formFactory,
            $dataObjectProcessor,
            $metadataFormFactory,
            $customerRepository,
            $extensibleDataObjectConverter,
            $data
        );
    }

    public function getSessionQuote(){
        return $this->_sessionQuote;
    }


    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->setTemplate('DI_GiftCards::order/create/form/account.phtml');
        return $this;
    }

}