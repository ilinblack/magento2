<?php

namespace DI\Documents\Model;

class Documents extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    protected function _construct()
    {
        $this->_init('DI\Documents\Model\ResourceModel\Documents');
    }
    public function getAvailableStatuses()
    {
        $availableOptions = ['1' => 'Enable', '0' => 'Disable'];

        return $availableOptions;
    }
}
