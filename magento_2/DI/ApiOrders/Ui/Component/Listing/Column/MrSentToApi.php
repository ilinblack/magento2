<?php

namespace DI\ApiOrders\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class PrebookStatus
 */
class MrSentToApi implements OptionSourceInterface
{

    const PRE_BOOK_YES=1;
    const PRE_BOOK_NO=0;

    public static function getOptionArray()
    {
        return [
            self::PRE_BOOK_YES => __('Yes'),
            self::PRE_BOOK_NO => __('No')
        ];
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $res = [];
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }
}