<?php

namespace DI\PayerStatus\Ui\Component\Listing\Columns;

use Magento\Framework\Escaper;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class Verified extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        \Magento\Framework\App\ResourceConnection $resource

    )
    {
        $this->resource = $resource;
        $this->connection =$this->resource->getConnection();
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $id = $item['entity_id'];
                $verify = null;

                $sql = "SELECT additional_information FROM sales_order_payment WHERE parent_id = $id;";
                $data = $this->connection->query($sql);
                $dataJson = $data->fetchAll();
                if(!empty($dataJson)){
                    $dataArray = json_decode($dataJson[0]['additional_information']);

                    if(array_key_exists('paypal_payer_status', $dataArray)){

                        foreach ($dataArray as $data){
                            if($data === "unverified"){
                                $verify = 0;
                            }
                        }
                    }
                }
                $attribute = 1;

                if($verify === 0){
                    $attribute = 0;
                }

                $item[$fieldName] = $attribute;

            }
        }
        return $dataSource;
    }
}