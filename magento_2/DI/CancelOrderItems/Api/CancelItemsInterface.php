<?php
namespace DI\CancelOrderItems\Api;

use DI\CancelOrderItems\Api\CancelItemsInformationInterface;

interface CancelItemsInterface {

    public function execute(
        string  $orderId,
        string  $items
    );
}