<?php

namespace DI\SDS\Block\Adminhtml\Grid\Edit;


class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    protected $_systemStore;
    protected $request;
    protected $_helper;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,

        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Backend\Helper\Data $mageHelper,


        array $data = []
    )
    {
        $this->_helper = $mageHelper;
        $this->request = $request;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }


    protected function _prepareForm()
    {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $model = $this->_coreRegistry->registry('row_data');
        $field = $this->_formFactory->create(

            ['data' => [
                'id' => 'edit_form',
                'enctype' => 'multipart/form-data',
                'action' => $this->getData('action'),
                'method' => 'post'
            ]
            ]
        );

        $field->setHtmlIdPrefix('mrgrid_');
        if ($model->getSDSId()) {
            $fieldset = $field->addFieldset(
                'base_fieldset',
                ['legend' => __('Edit Category'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('sds_id', 'hidden', ['name' => 'sds_id']);
        } else {
            $fieldset = $field->addFieldset(
                'base_fieldset',
                ['legend' => __('Add Row Data'), 'class' => 'fieldset-wide']
            );
        }

        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);

        $fieldset->addField(
            'category',
            'text',
            [
                'name' => 'category',
                'label' => __('Category'),
                'id' => 'category',
                'title' => __('Category'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'file_name',
            'text',
            [
                'name' => 'file_name',
                'label' => __('SDS Name'),
                'id' => 'file_name',
                'title' => __('File name'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField('mr_sds_file', 'file', array(
            'label'     => 'PDF File To Import',
            'required'  => false,
            'name'      => 'mr_sds_file',
        ));
        
        $field->setValues($model->getData());

        $field->setUseContainer(true);
        $this->setForm($field);
    }
}

