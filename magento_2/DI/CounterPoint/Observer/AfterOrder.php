<?php
namespace DI\CounterPoint\Observer;
class AfterOrder implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \DI\CounterPoint\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger

    )
    {
        $this->helper = $helper;
        $this->logger = $logger;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            $grossProfitArray=[];
            if($order) {

                foreach ($order->getItemsCollection() as $item) {

                    $product = $item->getProduct();
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    if (!$product){
                        $product =
                            $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
                    }

                    $price = $product->getPrice();
                    $cost = $product->getCost();
                    if ($cost || $price) {
                        $grossProfit = strval(str_replace("%", "", $this->helper->calculateGrossProfit($cost, $price)));
                        $item->setData('gross_profit', $grossProfit);
                        $grossProfitArray[] = $grossProfit;
                    }
                }
                $avarageGP = $this->helper->calculateAvarageGP($grossProfitArray);
                $order->setAverageGrossProfit($avarageGP);

            }

        }catch (\Exception $ex){

            // please check logs.
        }
    }


}
