<?php

namespace DI\GiftCards\Controller\Customer;

class Apply extends \Magento\Framework\App\Action\Action
{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \DI\GiftCards\Helper\Data $giftCardHelper
    )
    { 
        $this->giftCardHelper = $giftCardHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $giftCardCode = $this->getRequest()->getParam('gift_card_code');
            $this->giftCardHelper->setGiftcardToCustomer($giftCardCode);
            $this->messageManager->addSuccess('Gift Card is applied');
        } catch (\Exception  $e) {
            $this->messageManager->addError($e->getMessage());
        }
        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }
}
