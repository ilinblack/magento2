<?php

namespace DI\AgeVerification\Block\Adminhtml\Verification;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\Address\Mapper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\Customer;

class Age extends \Magento\Customer\Block\Adminhtml\Edit\Tab\View\PersonalInfo
{

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        AccountManagementInterface $accountManagement,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerDataFactory,
        \Magento\Customer\Helper\Address $addressHelper,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Framework\Registry $registry,
        Mapper $addressMapper,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Customer\Model\Logger $customerLogger,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\User\Model\UserFactory $userFactory,
        \DI\AgeVerification\Model\AgeVerification $ageVerification,
        $data = []
)
    {
        $this->ageVerification = $ageVerification;
        $this->authSession = $authSession;
        $this->userFactory = $userFactory;
        parent::__construct($context,$accountManagement,$groupRepository,$customerDataFactory,$addressHelper,$dateTime,$registry,$addressMapper,$dataObjectHelper,$customerLogger,$data);
    }


    public function getCurrentUserId()
    {
        return $this->authSession->getUser()->getId();
    }

    public function getCustomerId()
    {
       return $this->getCustomer()->getId();
    }

    public function getComments()
    {
       return $this->ageVerification->getAgeVerificationByCustomerId($this->getCustomerId());
    }

    public function getUserData($userId)
    {
        $user = $this->userFactory->create()->load($userId);
        $data['username'] = $user->getData('username');
        $data['firstname'] = $user->getData('firstname');
        $data['lastname'] = $user->getData('lastname');
        return $data;
    }
}
