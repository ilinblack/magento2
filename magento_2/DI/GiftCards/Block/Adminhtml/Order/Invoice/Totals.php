<?php

namespace DI\GiftCards\Block\Adminhtml\Order\Invoice;

use Magento\Sales\Model\Order\Invoice;

class Totals extends \Magento\Sales\Block\Adminhtml\Order\Invoice\Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        if (!$this->getSource()->getIsVirtual() && $this->getSource()->getOrder()->getGiftCardValue() != 0
        ) {
            $this->_totals['giftcards'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'giftcards',
                    'value' => '-' . $this->getSource()->getOrder()->getGiftCardValue(),
                    'base_value' => '-' . $this->getSource()->getOrder()->getGiftCardValue(),
                    'label' => __('Gift Card value'),
                ]
            );
        }
        return $this;
    }
}
