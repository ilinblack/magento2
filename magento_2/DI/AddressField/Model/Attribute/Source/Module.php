<?php
class DI_AddressField_Model_Attribute_Source_Module extends Mage_Eav_Model_Entity_Attribute_Source_Abstract{
    protected $_options = null;

    public function getAllOptions($withEmpty = false){
        if (is_null($this->_options)){
            $this->_options = array();
            $this->_options[] = array('label'=> 'Please select your destination type', value=>'default');
            $this->_options[] = array('label'=> 'Residential', value=>'residential');
            $this->_options[] = array('label'=> 'Commercial', value=>'commercial');
        }
        $options = $this->_options;
        if ($withEmpty) {
            array_unshift($options, array('value'=>'', 'label'=>''));
        }
        return $options;
    }
    public function getOptionText($value)
    {
        $options = $this->getAllOptions(false);

        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }
}