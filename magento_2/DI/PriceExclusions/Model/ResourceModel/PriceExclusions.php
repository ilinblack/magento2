<?php
namespace DI\PriceExclusions\Model\ResourceModel;

class PriceExclusions extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('customer_price_exclusions', 'entity_id');
    }

}