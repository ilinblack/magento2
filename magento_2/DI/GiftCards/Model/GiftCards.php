<?php

namespace DI\GiftCards\Model;

class GiftCards extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    private function getApiLogin()
    {
        return 'N3q2JCnLRLDwX9m8Fx5F';
        return $this->scopeConfig->getValue(self::XML_CONFIG_LOGIN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    private function getApiPassword()
    {
        return 'NUAU8oCobk35PxAtX6fu';
        return $this->scopeConfig->getValue(self::XML_CONFIG_LOGIN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function api()
    {
        \DI\LocationBasedInventory\lib\Api::login($this->getApiLogin(), $this->getApiPassword(), false);
        return \DI\LocationBasedInventory\lib\Api::getInstance();
    }

    public function getGiftCard($number)
    {
        if (!$number) {
            return ['GiftCardNo' => null, 'Value' => 0];
        }
        $api = $this->api();
        $giftcard = $api::GET('/giftcard/' . $number);
        return $giftcard['GiftCard'];
    }

    public function redeemGiftCard($number, $params)
    {
        $api = $this->api();
        $giftcard = $api::POST('/giftcard/redeem/' . $number, $params);
        return $giftcard['GiftCard'];
    }


}