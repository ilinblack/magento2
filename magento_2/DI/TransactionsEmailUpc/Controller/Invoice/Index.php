<?php
namespace DI\TransactionsEmailUpc\Controller\Invoice;


class Index extends \Magento\Framework\App\Action\Action{
    protected $_invoiceSender;

    protected $_invoice;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        \Magento\Sales\Model\Order\Invoice $invoice
    )
    {
        parent::__construct($context);
        $this->_invoiceSender = $invoiceSender;
        $this->_invoice = $invoice;
    }
    public function execute(){

        $invoice_id = '6';
        $invoiceData = $this->_invoice->load($invoice_id);
        $result = $this->_invoiceSender->send($invoiceData);

    }

}

