<?php

class DI_AddressField_Block_Directory_Data extends Mage_Directory_Block_Data{

    public function getDestinationType(){

        $value = array( "default" => "Please select your destination type",
            "residential"=>"Residential",
            "commercial" =>"Commercial");

        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName('destination_type')
            ->setId('destination_type')
            ->setTitle(Mage::helper('checkout')->__('Destination Type'))
            ->setClass('validate-select')
            ->setOptions($value);

        return $select->getHtml();
    }

}