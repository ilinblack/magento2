<?php

namespace DI\AgeVerification\Model;


class Age extends \Magento\Framework\Model\AbstractModel implements \DI\AgeVerification\Api\Data\ApiInterface
{
    /**
     * Define main table
     */

    const CACHE_TAG = 'customer_age_verification';

    protected $_cacheTag = 'customer_age_verification';

    protected $_eventPrefix = 'customer_age_verification';

    protected function _construct()
    {
        $this->_init('DI\AgeVerification\Model\ResourceModel\Age');

    }

    public function getId()
    {
        return $this->getData(self::ID);
    }

    public function setId($Id)
    {
        return $this->setData(self::ID, $Id);
    }

    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    public function getUserId()
    {
        return $this->getData(self::USER_ID);
    }

    public function setUserId($userId)
    {
        return $this->setData(self::USER_ID, $userId);
    }

    public function getIsAgeVerificationPassed()
    {
        return $this->getData(self::IS_AGE_VERIFICATION_PASSED);
    }

    public function setIsAgeVerificationPassed($verification)
    {
        return $this->setData(self::IS_AGE_VERIFICATION_PASSED, $verification);

    }

    public function getComment()
    {
        return $this->getData(self::COMMENT);
    }

    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

}