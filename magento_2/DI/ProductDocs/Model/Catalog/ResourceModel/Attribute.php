<?php
namespace DI\ProductDocs\Model\Catalog\ResourceModel;

class Attribute extends \Magento\Catalog\Model\ResourceModel\Attribute
{
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object){
        if ($object->getFrontendInput() === 'file'){
            $object->setBackendType('varchar');
            $object->setBackendModel('DI\ProductDocs\Model\Attribute\Backend\File');
        }
        return parent::_beforeSave($object);
    }
}