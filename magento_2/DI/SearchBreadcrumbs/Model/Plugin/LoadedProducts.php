<?php

namespace DI\SearchBreadcrumbs\Model\Plugin;

class LoadedProducts
{

    public function __construct
    (
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        $this->resource = $resource;
    }

    public function afterGetLoadedProductCollection(\Magento\Catalog\Block\Product\ListProduct $subject, $result)
    {


        $storeId = $this->_storeManager->getStore()->getId();


            if ($result) {
                try {
                    $connection = $this->resource->getConnection();
                    /**
                     * Find All Product Skus
                     */

                    $productSkus = [];
                    foreach ($result->getItems() as $item) {
                        $productSkus[] = $item->getSku();
                    }

                    /**
                     * Find All Longest Urls
                     */
                    $productUrls = [];
                    $skus = implode(',', $productSkus);
                    if ($skus && $connection) {
                        $sql = "SELECT ur.*, cpe.sku FROM `url_rewrite` as ur
                            LEFT JOIN `catalog_product_entity` as cpe ON cpe.entity_id = ur.entity_id
                            WHERE ur.entity_type LIKE '%product%' AND ur.store_id = '2' AND cpe.sku IN ($skus)";

                        $urls = $connection->fetchAll($sql);
                        foreach ($urls as $url) {
                            $sku = $url['sku'];
                            $requestPath = $url['request_path'];
                            if (!isset($productUrls[$sku])) {
                                $productUrls[$sku] = $requestPath;
                            } elseif ((substr_count($productUrls[$sku], '/')) < (substr_count($requestPath, '/'))) {
                                $productUrls[$sku] = $requestPath;
                            }
                        }
                    }

                    /**
                     * Update Products Urls
                     */
                    foreach ($productUrls as $key => $url) {
                        foreach ($result->getItems() as $item) {
                            if ($item->getSku() == $key) {
                                $item->setData('request_path', $url);
                            }
                        }
                    }
                } catch (\Exception $ex) {

                }
            }

        return $result;
    }

}