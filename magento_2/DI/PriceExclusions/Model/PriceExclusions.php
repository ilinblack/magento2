<?php
namespace DI\PriceExclusions\Model;

class PriceExclusions extends \Magento\Framework\Model\AbstractModel implements \DI\PriceExclusions\Api\Data\ApiInterface
{
    const CACHE_TAG = 'customer_price_exclusions';

    protected $_cacheTag = 'customer_price_exclusions';

    protected $_eventPrefix = 'customer_price_exclusions';

    protected function _construct()
    {
        $this->_init('DI\PriceExclusions\Model\ResourceModel\PriceExclusions');
    }

    public function getId()
    {
        return $this->getData(self::ID);
    }

    public function setId($Id)
    {
        return $this->setData(self::ID, $Id);
    }

    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    public function getRecordId()
    {
        return $this->getData(self::RECORD_ID);
    }

    public function setRecordId($recordId)
    {
        return $this->setData(self::RECORD_ID, $recordId);
    }

    public function getCustomerGroupIds()
    {
        return $this->getData(self::CUSTOMER_GROUP_IDS);
    }

    public function setCustomerGroupIds($customerGroupIds)
    {
        return $this->setData(self::CUSTOMER_GROUP_IDS, $customerGroupIds);
    }

    public function getBrandId()
    {
        return $this->getData(self::BRAND_ID);
    }

    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }

}