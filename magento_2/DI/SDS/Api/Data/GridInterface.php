<?php

namespace DI\SDS\Api\Data;

interface GridInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const SDS_ID = 'sds_id';
    const CATEGORY = 'category';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const FILE_PATH = 'file_path';
    const FILE_NAME = 'file_name';

    /**
     * Get SDSId.
     *
     * @return int
     */
    public function getSDSId();

    /**
     * Set SDSId.
     */
    public function setSDSId($SDSId);


    /**
     * Get Category.
     *
     * @return varchar
     */
    public function getCategory();


    /**
     * Set Category.
     */
    public function setCategory($category);


    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();


    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);


    /**
     * Get UpdatedAt.
     *
     * @return varchar
     */
    public function getUpdatedAt();

    /**
     * Set UpdateTime.
     */
    public function setUpdatedAt($updatedAt);
    /**
     * Get FilePath.
     *
     * @return varchar
     */
    public function getFilePath();

    /**
     * Set FilePath.
     */
    public function setFilePath($file_path);
    /**
     * Get FileName.
     *
     * @return varchar
     */
    public function getFileName();

    /**
     * Set FileName.
     */
    public function setFileName($file_name);


}