<?php
namespace DI\Documents\Block\Adminhtml;

class Documents extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'DI_Documents';
        $this->_controller = 'adminhtml_DIdocs';

        parent::_construct();
    }
}