<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace DI\TestModule\Plugin\Model\Product;

use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Session as CustomerSession;

class ProductName
{

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @param CustomerSession $customerSession
     */
    public function __construct(
        CustomerSession $customerSession
    )
    {
        $this->customerSession = $customerSession;
    }


    /**
     * @param Product $subject
     * @param  string $result
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */


    public function afterGetName(Product $subject, $result)
    {
        /**
            Change product name for logged in customers.
         */

        if ($this->customerSession->getCustomerId()) {

            $productName = $result;

            $result = 'Hello World ' . $productName;

        }

        return $result;

    }

}
