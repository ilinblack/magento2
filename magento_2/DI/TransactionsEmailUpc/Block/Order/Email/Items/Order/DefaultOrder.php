<?php
namespace DI\TransactionsEmailUpc\Block\Order\Email\Items\Order;

class DefaultOrder extends \Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder
{

    public function getUpc($item)
    {
        if($item->getProduct()->getTypeId() === 'simple'){
            return $item->getProduct()->getUpc();
        }elseif ($item->getProduct()->getTypeId() === 'configurable'){
            $upc = null;
            $simples = $item->getProduct()->getTypeInstance()->getUsedProducts($item->getProduct());
            foreach($simples as $simple ){
                $upc = $simple->getUpc();
                return $upc;
            }
            return $upc;
        }

    }
}