<?php
namespace DI\AccountByGsap\Controller\Account;

class Registration extends \Magento\Framework\App\Action\Action{

    protected $_customerFactory;
    protected $_customerRepositoryInterface;
    protected $_addressFactory;
    protected $_resultJsonFactory;
    protected $_messageManager;
    protected $_encryptor;
    protected $_customerRegistry;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\AddressFactory $addressFactory
    )
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_customerFactory = $customerFactory;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_addressFactory = $addressFactory;
        $this->_messageManager = $messageManager;
        $this->_encryptor = $encryptor;
        $this->_customerRegistry = $customerRegistry;
        parent::__construct($context);
    }
    public function execute()
    {

        if ($this->getRequest()->isAjax()) {

            $data = $this->getRequest()->getPost();

            $result = $this->_resultJsonFactory->create();
            $error = false;
            $message = '';

            if (!\Zend_Validate::is(trim($data['email']), 'EmailAddress')) {
                $error = true;
                $message = "Please. Enter your email like in ex:johndoe@mail.com";
            }
            if (!\Zend_Validate::is(trim($data['password']), 'NotEmpty')) {
                $error = true;
                $message = "Please. Enter your password.";
            }
            if (!\Zend_Validate::is(trim($data['passwordConfirmation']), 'NotEmpty')) {
                $error = true;
                $message = "Please. Confirm your password.";
            }
            if (!\Zend_Validate::is(trim($data['street']), 'NotEmpty')) {
                $error = true;
                $message = "Please, enter your street.";
            }
            if (!\Zend_Validate::is(trim($data['city']), 'NotEmpty')) {
                $error = true;
                $message = "Please, enter your city.";
            }
            if (!\Zend_Validate::is(trim($data['phone']), 'NotEmpty')) {
                $error = true;
                $message = "Please, enter your phone.";
            }
            if(trim($data['password']) != trim($data['passwordConfirmation'])){
                $error = true;
                $message = "Password confirmation is failed.Please, try again.";
            }
                if (!$error) {
                   $verify = $this->setCustomerDefinition($data);
                   if($verify){
                       $this->_messageManager->addSuccess('Thanks for verify your account. Please, enter site using your email and password.');
                       return $result->setData('success');
                   }else{
                       $this->_messageManager->addError('Something went wrong. Please,try later.');
                       return $result->setData('fail');
                   }

                }else{
                    $this->_messageManager->addError($message);
                    return $result->setData('false');
                }

        }
    }
    private function setCustomerDefinition($data)
    {

        $customerCollection = $this->_customerFactory->create();

        foreach ($customerCollection as $customer) {

            $tokenHash = $this->_encryptor->getHash($customer->getName(), true);
            if ($customer->getId() === $data['customerId']) {
                $customer->setEmail($data['email']);
                $customer->setPassword($data['password']);
                $customer->setData('rp_token', $tokenHash);
                $customer->setData('rp_token_created_at', time());
                $customer->setPassword($data['password']);
                $customer->setServiceStationName($data['ssn']);
                $customer->setServiceStationPhone($data['ssph']);
                $customer->setGsap($data['gsap_registration']);

                $firstName =$customer->getName();
                $lastName = $customer->getName();

                $addresses = $this->_addressFactory->create()->getCollection()->setCustomerFilter($customer)->load();
                //die(var_dump(count($addresses)));
                if(count($addresses) < 1){

                    $addresses = $this->_addressFactory->create()
                    ->setCustomerId($customer->getId())
                    ->setFirstname($firstName)
                    ->setLastname($lastName)
                    ->setStreet($data['street'])
                    ->setDistrict($data['district'])
                    ->setCity($data['city'])
                    ->setRegion($data['region'])
                    ->setCountryId("GB")
                    ->setPostcode($data['postcode'])
                    ->setTelephone($data['phone']);
                    $addresses->save();
                }else{

                        foreach ($addresses as $address){
                            $address->setFirstname($firstName);
                            $address->setLastname($lastName);
                            $address->setStreet($data['street']);
                            $address->setDistrict($data['district']);
                            $address->setCity($data['city']);
                            $address->setRegion($data['region']);
                            $address->setCountryId("GB");
                            $address->setPostcode($data['postcode']);
                            $address->setTelephone($data['phone']);
                            $address->save();
                     }
                }
                $customer->setConfirmation(null);
                $customer->setIsDefined(1);
                $customer->save();

            }

        }
        return true;
    }


}