<?php

namespace DI\Documents\Controller\Adminhtml\Documents;

use Magento\Framework\App\Filesystem\DirectoryList;

class Upload extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'DI_Documents::docs';

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \DI\Documents\Model\Documents
     */
    protected $_model;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \DI\Documents\Model\Documents $model
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->_model = $model;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        try {

            $productEditCheck = false;

            if(isset($_FILES['product'])){

                $newFileArray = array();

                foreach($_FILES['product'] as $k=>$value){
                    $newFileArray[$k] = $value['path'];
                }

                unset($_FILES['product']);
                $_FILES['path'] = $newFileArray;

                $productEditCheck = true;

            }

            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                ['fileId' => 'path']
            );
            $uploader->setAllowedExtensions(['pdf']);
            /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
            //$imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
            //$uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');

            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(DirectoryList::MEDIA);

            $config = $this->_objectManager->get('Magento\Catalog\Model\Product\Media\Config');
            $result = $uploader->save($mediaDirectory->getAbsolutePath($config->getBaseTmpMediaPath()));

            unset($result['tmp_name']);
            unset($result['path']);


            if($productEditCheck){

                /** @var \DI\Documents\Model\Documents $model */
                $model = $this->_model;

                $data['doc_name'] = $data['file_name'] = $result['name'];
                $data['path'] = $result['file'];
                $data['doc_size'] = $result['size'];

                $model->setData($data);

                $model->save();

                $result['id'] = $model->getId();

            }


            $result['url'] = $this->_objectManager->get('Magento\Catalog\Model\Product\Media\Config')->getTmpMediaUrl($result['file']);

            $result['file'] = $result['file'] . '.tmp';



        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));
        return $response;
    }
}