<?php

namespace DI\GiftCards\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class SalesModelServiceQuoteSubmitBefore implements ObserverInterface
{
    protected $giftCardHelper;

    public function __construct(
        \DI\GiftCards\Helper\Data $giftCardHelper,
        \Magento\Framework\App\State $state
    )
    {
        $this->giftCardHelper = $giftCardHelper;
        $this->_state = $state;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $order = $observer->getEvent()->getOrder();

            $order->setData('gift_card_value', $observer->getEvent()->getQuote()->getData('gift_card_value'));
            $giftCardNumber = $this->giftCardHelper->getCustomerGiftCardNumber();
            $order->setData('gift_card_number', $giftCardNumber);
            return $this;
    }
}
