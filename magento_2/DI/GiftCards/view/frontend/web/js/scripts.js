require(['jquery'], function ($) {
    $(function () {
        $(document).ready(function () {
            //customer popup information

            $('.overlay_information .close_btn').click(function(){
                closeInformation();
                $('.overlay_information').fadeOut();
            })

            $('.overlay_information .apply').click(function(){
                closeInformation();
            })

            $(document).click(function (event) {
                if ($(event.target).closest('.overlay_content_information').length == 0 && $(event.target).closest('.overlay_information').length != 0) {
                    closeInformation();
                    $('.overlay_information').fadeOut();
                }
            });
            $('.overlay_information a.parent').click(function(){
                if(innerWidth < 1025){
                    return false;
                }
            })

            // function closeInformation(){
                // createCookie('close_information_popup','true',1);
            // }
 
            function createCookie(name,value,days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days*24*60*60*1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + value + expires + "; path=/";
            }
            
            closeInformation = function (){
                createCookie('close_information_popup','true',1);
            }

            //checkout popup information
            $('.overlay_checkout_information .close_btn').click(function(){
                $('.overlay_checkout_information').fadeOut();
            })

            $('.overlay_checkout_information .skip_btn').click(function(){
                $('.overlay_checkout_information').fadeOut();
                return false;
            })
            // $('.overlay_checkout_information .apply').click(function(){
            //     closeInformation();
            // })

            $(document).click(function (event) {
                if ($(event.target).closest('.overlay_checkout_content_information').length == 0 && $(event.target).closest('.overlay_checkout_information').length != 0) {
                    // closeInformation();
                    $('.overlay_checkout_information').fadeOut();
                }
            });
        })
    });


});