<?php
namespace DI\TransactionsEmailUpc\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action{
    protected $_order;
    protected $_orderSender;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\Order $_order,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
    )
    {
        parent::__construct($context);
        $this->_order = $_order;
        $this->_orderSender = $orderSender;
    }
    public function execute(){

        $orderid = '56';
        $emailSender = $this->_orderSender;
        $order = $this->_order->load($orderid);
        $result = $emailSender->send($order);

    }

}
