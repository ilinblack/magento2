<?php

namespace DI\SearchBreadcrumbs\Model\Plugin;

use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\Layer\Resolver as LayerResolver;
use Magento\CatalogSearch\Helper\Data;
use Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Search\Model\QueryFactory;

class SearchCollection extends \Magento\CatalogSearch\Block\Result
{
    protected $productCollection;
    protected $catalogSearchData;
    protected $catalogLayer;
    private $queryFactory;
    protected $resource;

    public function __construct(
        Context $context,
        LayerResolver $layerResolver,
        Data $catalogSearchData,
        QueryFactory $queryFactory,
        array $data = [],
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource
    )

    {
        $this->catalogLayer = $layerResolver->get();
        $this->catalogSearchData = $catalogSearchData;
        $this->queryFactory = $queryFactory;
        $this->_storeManager = $storeManager;
        $this->resource = $resource;

        parent::__construct($context, $layerResolver, $catalogSearchData, $queryFactory, $data);
    }

    public function getTemplate()
    {
        return 'Magento_CatalogSearch::result.phtml';
    }
 
    protected function _getProductCollection()
    {


        $storeId = $this->_storeManager->getStore()->getId();

        if (null === $this->productCollection) {
            $this->productCollection = $this->getListBlock()->getLoadedProductCollection();
        }





            if ($this->productCollection) {
                try {
                    $connection = $this->resource->getConnection();
                    /**
                     * Find All Product Skus
                     */

                    $productSkus = [];
                    foreach ($this->productCollection->getItems() as $item) {
                        $productSkus[] = $item->getSku();
                    }

                    /**
                     * Find All Longest Urls
                     */
                    $productUrls = [];
                    $skus = implode(',', $productSkus);
                    if ($skus && $connection) {
                        $sql = "SELECT ur.*, cpe.sku FROM `url_rewrite` as ur
                                LEFT JOIN `catalog_product_entity` as cpe ON cpe.entity_id = ur.entity_id
                                WHERE ur.entity_type LIKE '%product%' AND ur.store_id = '2' AND cpe.sku IN ($skus)";

                        $urls = $connection->fetchAll($sql);
                        foreach ($urls as $url) {
                            $sku = $url['sku'];
                            $requestPath = $url['request_path'];
                            if (!isset($productUrls[$sku])) {
                                $productUrls[$sku] = $requestPath;
                            } elseif ((substr_count($productUrls[$sku], '/')) < (substr_count($requestPath, '/'))) {
                                $productUrls[$sku] = $requestPath;
                            }
                        }
                    }

                    /**
                     * Update Products Urls
                     */
                    foreach ($productUrls as $key => $url) {
                        foreach ($this->productCollection->getItems() as $item) {
                            if ($item->getSku() == $key) {

                                $item->setData('request_path', $url);
                            }
                        }
                    }
                } catch (\Exception $ex) {

                }
            }

        return $this->productCollection;
    }


}
