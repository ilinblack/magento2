<?php

namespace DI\GiftCards\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $customerSetupFactory;

    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $salesOrderTable = $installer->getTable('sales_order');
        $salesInvoiceTable = $installer->getTable('sales_invoice');
        $salesCreditmemoTable = $installer->getTable('sales_creditmemo');

        $quoteTable = $installer->getTable('quote');

        $columns = [
            'gift_card_value' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'default' => 0,
                'comment' => 'Gift Card Value',
            ],
            'gift_card_number' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'default' => 0,
                'comment' => 'Gift Card Number',
            ],
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            $connection->addColumn($salesOrderTable, $name, $definition);
//            if ('gift_card_number' == $name) continue;

            $connection->addColumn($salesInvoiceTable, $name, $definition);
            $connection->addColumn($salesCreditmemoTable, $name, $definition);
            $connection->addColumn($quoteTable, $name, $definition);
        }

        /**
         * Add attributes to Customer
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'gift_card_value',
            [
                'type' => 'varchar',
                'label' => 'Gift Card Value',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 255,
                'system' => 0,
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'gift_card_number',
            [
                'type' => 'varchar',
                'label' => 'Gift Card Number',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 255,
                'system' => 0,
            ]
        );

        $giftCardValue = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'gift_card_value');
        $giftCardValue->setData(
            'used_in_forms',
            ['adminhtml_customer']

        );
        $giftCardValue->save();

        $giftCardNumber = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'gift_card_number');
        $giftCardNumber->setData(
            'used_in_forms',
            ['adminhtml_customer']

        );
        $giftCardNumber->save();
 
        $installer->endSetup();
    }
}