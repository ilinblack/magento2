<?php

namespace DI\SpecialPrice\Pricing\Price;

class SpecialPrice extends \Magento\Bundle\Pricing\Price\SpecialPrice
{

    protected $value;

    /**
     * New SpecialPrice Class for Bundles
     *
     * */

    public function getValue()
    {

        $this->value = $this->getDiscountPercent();

        //here we return float value that is final product price

        return $this->value;

    }
}