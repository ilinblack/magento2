<?php

namespace DI\Documents\Model\ResourceModel;

class Documents extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('DI_documents', 'doc_id');
    }
}