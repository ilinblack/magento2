<?php
namespace DI\Documents\Block\Adminhtml\Documents;

use	Magento\Backend\Block\Widget\Grid as WidgetGrid;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /***@var \DI\Documents\Model\Resource\Documents\Collection
     */
    protected	$_documentsCollection;

    /**
     *	@param	\Magento\Backend\Block\Template\Context	$context
     *	@param	\Magento\Backend\Helper\Data $backendHelper
     *	@param \DI\Documents\Model\ResourceModel\Subscription\Collection $subscriptionCollection
     *	@param	array $data
     */
    public	function	__construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \DI\Documents\Model\ResourceModel\Documents\Collection $documentsCollection,
        array $data	= []
    )	{
        $this->_documentsCollection = $documentsCollection;
        parent::__construct($context, $backendHelper, $data);
        $this->setEmptyText(__('No Documents Found'));
    }
    /**
     *	Initialize	the	subscription	collection
     *
     *	@return	WidgetGrid
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->_documentsCollection);
        return parent::_prepareCollection();
    }

}