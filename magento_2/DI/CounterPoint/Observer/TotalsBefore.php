<?php

namespace DI\CounterPoint\Observer;

class TotalsBefore implements \Magento\Framework\Event\ObserverInterface
{
    protected $_pricingHelper;
    protected $_state;

    public function __construct(
        \Magento\Framework\Pricing\Helper\Data $helper,
        \Magento\Framework\App\State $state,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Backend\Model\Session\Quote $backendQuoteSession,
        \DI\CounterPoint\Helper\Data $gpHelper
    )
    {
        $this->_pricingHelper = $helper;
        $this->_state = $state;
        $this->_request = $request;
        $this->_backendQuoteSession = $backendQuoteSession;
        $this->gpHelper = $gpHelper;

    }

    private function isAdminArea()
    {
        return 'adminhtml' === $this->_state->getAreaCode();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
            if ($this->_request->getModuleName() == 'quotation') return $this;

            if (!$this->isAdminArea()) {
                return $this;
            }
            $flag = true;
            $quote = $observer->getEvent()->getQuote();

            foreach ($quote->getAllItems() as $item) {
                $product = $item->getProduct();
                $cost = $product->getCost();


                $customPrice = $item->getCustomPrice();
                $newCustomPrice = $item->getNewCustomPrice();
                $grossProfitPrice = $item->getGrossProfit();
                $calculatedCustomPrice = $item->getCalculatedCustomPrice();
                $attributeCustomPrice = $item->getAttributeCustomPrice();

                if ($product->getTypeId() == 'bundle') {
                    continue;
                }
                if ($item->getParentItem()) {
                    if ($grossProfitPrice !== null) {
                        $item->setCustomPrice($grossProfitPrice);
                        $item->setOriginalCustomPrice($grossProfitPrice);
                        $item->getProduct()->setIsSuperMode(true);
                    } elseif ($calculatedCustomPrice !== null) {

                        $item->setCustomPrice($calculatedCustomPrice);
                        $item->setOriginalCustomPrice($calculatedCustomPrice);
                        $item->getProduct()->setIsSuperMode(true);
                    } elseif ($attributeCustomPrice !== null) {

                        $item->setCustomPrice($attributeCustomPrice);
                        $item->setOriginalCustomPrice($attributeCustomPrice);
                        $item->getProduct()->setIsSuperMode(true);
                    }
                } else {
                    if ($customPrice != $newCustomPrice) {
                        $item->setGrossProfit(null);
                        $item->setCalculatedCustomPrice(null);
                        $item->setAttributeCustomPrice(null);

                    } else {
                        if (!$item->getParentItem() && $product->getSpecialPrice() && $product->getData('special_to_date') >= date('Y-m-d') || !$product->getData('special_to_date') && $flag) {
                            $item->setCustomPrice($product->getSpecialPrice());
                        }

                        if ($grossProfitPrice !== null) {

                            $item->setCustomPrice($grossProfitPrice);
                            $item->setOriginalCustomPrice($grossProfitPrice);
                            $item->getProduct()->setIsSuperMode(true);

                        } elseif ($calculatedCustomPrice !== null) {

                            $item->setCustomPrice($calculatedCustomPrice);
                            $item->setOriginalCustomPrice($calculatedCustomPrice);
                            $item->getProduct()->setIsSuperMode(true);

                        } elseif ($attributeCustomPrice !== null) {

                            $item->setCustomPrice($attributeCustomPrice);
                            $item->setOriginalCustomPrice($attributeCustomPrice);
                            $item->getProduct()->setIsSuperMode(true);
                        }
                    }

                    $item->setNewCustomPrice($item->getCustomPrice());
                }
                if($item->getCustomPrice()) {
                        $gpPercent = $this->gpHelper->calculateGrossProfit($cost, $item->getCustomPrice());
                        $item->setGrossProfitPercent($gpPercent);
                }

                $item->save();
            }
        return $this;
    }
}