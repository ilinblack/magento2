<?php

namespace DI\SalePrice\Cron;

class Check{


    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \DI\SalePrice\Helper\Data $helper,
        \Magento\Framework\Event\Manager $eventManager

    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->helper = $helper;
        $this->_eventManager = $eventManager;
    }

    public function execute()
    {
        try {
            $collection = $this->_productCollectionFactory->create();

            $collection->addAttributeToSelect(['price','sale_price','special_price'])
                ->addAttributeToFilter('sale_price', ['notnull' => true]);

            if (count($collection) > 0) {
                foreach ($collection as $product) {

                    $this->_eventManager->dispatch("calculate_sale_price", array("product" => $product));

                }
            }
        }catch (\Exception $e){
        }
    }
}