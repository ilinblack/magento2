<?php

namespace DI\AgeVerification\Api\Data;

interface ApiInterface
{

    const ID = 'entity_id';
    const CUSTOMER_ID = 'customer_id';
    const USER_ID = 'user_id';
    const IS_AGE_VERIFICATION_PASSED = 'is_age_verification_passed';
    const COMMENT = 'comment';
    const CREATED_AT = 'created_at';

    public function getId();

    public function setId($Id);


    public function getCustomerId();

    public function setCustomerId($customerId);


    public function getUserId();

    public function setUserId($userId);


    public function getIsAgeVerificationPassed();

    public function setIsAgeVerificationPassed($verification);


    public function getComment();

    public function setComment($comment);


    public function getCreatedAt();

    public function setCreatedAt($createdAt);

}