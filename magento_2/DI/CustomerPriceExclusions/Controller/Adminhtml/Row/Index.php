<?php

namespace DI\CustomerPriceExclusions\Controller\Adminhtml\Row;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Customer\Controller\Adminhtml\Index
{

    public function execute()

    {

        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;

    }


}