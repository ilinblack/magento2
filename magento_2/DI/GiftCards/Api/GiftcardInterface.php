<?php

namespace DI\GiftCards\Api;

interface GiftcardInterface
{
    /**
    * set value.
    *
    * @param string $value
    * @return bool 
    * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
    * @throws \Magento\Framework\Exception\CouldNotSaveException The specified coupon could not be added.
    */
    public function set($value);
    /**
    * cancel.
    * @param int $cartId The cart ID.
    * @return bool
    * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
    * @throws \Magento\Framework\Exception\CouldNotSaveException The specified coupon could not be added.
    */
    public function cancel();
}