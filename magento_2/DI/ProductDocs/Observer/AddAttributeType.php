<?php

namespace DI\ProductDocs\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddAttributeType implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $response = $observer->getEvent()->getResponse();
        $types = $response->getTypes();
        $types[] = [
            'value' => 'file',
            'label' => __('File'),
        ];
        $response->setTypes($types);
        return $this;
    }
}