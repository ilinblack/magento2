<?php

namespace DI\GiftCards\Model\Checks;

use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;

class ZeroTotal extends \Magento\Payment\Model\Checks\ZeroTotal
{
    public function isApplicable(MethodInterface $paymentMethod, Quote $quote)
    {
        if (!($quote->getBaseGrandTotal() < 0.0001 && $paymentMethod->getCode() != 'giftcards')) {
            return 1;
        } elseif (!($quote->getBaseGrandTotal() < 0.0001 && $paymentMethod->getCode() != 'free')) {
            return 1;
        } else {
            return null;
        }
    }
}
