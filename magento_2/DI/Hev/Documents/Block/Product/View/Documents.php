<?php

namespace DI\Documents\Block\Product\View;

class Documents extends \Magento\Framework\View\Element\Template
{

    protected $_objectManager;

    protected $_registry;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $data);
    }

    public function _prepareLayout(){

        return parent::_prepareLayout();
    }

    /**
     * Get the current product
     *
     * @return Product
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }


    /**
     * Get store
     *
     * @return  object
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }



    public function getSelectedDocs(){
 	
		
        $ids = $this->getCurrentProduct()->getProductAttachmentDocuments();
		
		if ($this->getCurrentProduct()->getTypeId()=='bundle' && $this->getLayout()->getBlock('product.info.bundle.options.grouped')->getOption()){
			$ids = [];
			$products  = $this->getLayout()->getBlock('product.info.bundle.options.grouped')->getOption()->getSelections();
			foreach($products as $_product){
				  $_ids = explode(',',$_product->getProductAttachmentDocuments());
				  $ids = array_merge($ids,$_ids);
				
			}
			$ids = join(",",$ids);
		}
		
		//d($ids);
 

        if($ids){

            $collection = $this->_objectManager->create('DI\Documents\Model\Documents')->getCollection();

            $ids_arr = explode(',',$ids);
			
			

            $documents = $collection->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('doc_id',array('in'=>$ids_arr))->setOrder('file_name','ASC');
			
            if($documents->getSize()){ 

                return $documents;
            }

        }

        return false;

    }

}