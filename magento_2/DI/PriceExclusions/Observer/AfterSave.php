<?php

namespace DI\PriceExclusions\Observer;

class AfterSave implements \Magento\Framework\Event\ObserverInterface
{

    protected $connection;

    public function __construct(
        \DI\PriceExclusions\Model\PriceExclusionsFactory $priceExclusionsFactory,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->_priceExclusions = $priceExclusionsFactory;
        $this->resource = $resource;
        $this->connection =$this->resource->getConnection();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getControllerAction()->getRequest();
        $data = $request->getParams();
        if ($data['customer']) {
            $customer = $data['customer'];
            $customerId = $customer['entity_id'];

            $collection = $this->_priceExclusions->create()->getCollection()
                ->addFilter('customer_id', $customerId)->load();
            if (count($collection) > 0) {
                $checkArray = [];

                foreach ($collection->getItems() as $item) {
                    $checkArray[$item->getRecordId()] = $item->getId();
                }

                    $this->deleteColumns($checkArray);

            }

            if (array_key_exists('price_excl', $data)) {

                $priceExcl = $data['price_excl'];
                $i = 0 ;
                foreach ($priceExcl['dynamic-rows'] as $rows) {
                    foreach ($rows as $row) {

                        $rowData = $this->_priceExclusions->create();
                        $rowData->setCustomerId($customerId);
                        $rowData->setRecordId($i);
                        $rowData->setCustomerGroupIds($row['customer_group_ids']);
                        $rowData->setBrandId($row['brand_id']);

                        $rowData->save();

                        $i++;
                    }

                }
            }
        }
    }

    protected function deleteColumns($checkArray){

        foreach ($checkArray as $key) {
            $sql = "DELETE FROM customer_price_exclusions WHERE entity_id = $key";
            $this->connection->query($sql);
        }
    }

}