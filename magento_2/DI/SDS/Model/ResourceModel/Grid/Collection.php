<?php

namespace DI\SDS\Model\ResourceModel\Grid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'sds_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('DI\SDS\Model\Grid',
            'DI\SDS\Model\ResourceModel\Grid');
    }
}