<?php

namespace DI\SDS\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action\Context;
use Magento\Framework\File\Uploader;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Backend\App\Action
{

    protected $gridFactory;
    protected $uploader = null;
    protected $dir;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \DI\SDS\Model\GridFactory $gridFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        array $data = []
    ) {
        parent::__construct($context, $resultPageFactory, $resultLayoutFactory, $resultForwardFactory,$data);
        $this->gridFactory = $gridFactory;
        $this->dir = $dir;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if (!$data) {
            $this->_redirect('DI_sds/grid/addrow');
            return;
        }
        if(isset($_FILES['mr_sds_file']['name']) && $_FILES['mr_sds_file']['name'] != '') {

          try{

              $uploader = new Uploader('mr_sds_file');


              $path = $this->dir->getRoot() . "/pub/mr_sds/data";


                $file_name = $_FILES['mr_sds_file']['name'];

                $correct_f_name = $uploader->getCorrectFileName($file_name);

                $_path = "/mr_sds/data" . "/" . $correct_f_name;


                $uploader->setAllowedExtensions(array('pdf'));
                $uploader->setAllowRenameFiles(false);


                $uploader->setFilesDispersion(false);


                $uploader->save($path);

                $this->messageManager->addSuccess('File was successfully uploaded');


                $rowData = $this->gridFactory->create();
                $rowData->setCategory($data['category']);
                $rowData->setFileName($data['file_name']);
                $rowData->setFilePath($_path);

                $rowData->save();
            } catch (\Exception $e) {
                $this->messageManager->addError(__($e->getMessage()));
        }

        }
            if (isset($data['id'])) {
                $rowData->setSDSId($data['id']);
            }


            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));

        $this->_redirect('DI_sds/grid/index');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('DI::save');
    }
}