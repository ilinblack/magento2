<?php

namespace DI\GiftCards\Plugin;

use Magento\Framework\Exception\NoSuchEntityException;

class OrderGet
{
    protected $orderExtensionFactory;
    protected $orderItemExtensionFactory;
    
    public function __construct(
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory
    )
    {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->orderItemExtensionFactory = $orderItemExtensionFactory;
    }

    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    )
    {
       return $this->setCustomAttr($resultOrder);
    }

    protected function setCustomAttr(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();

        if ($extensionAttributes === null) {
            $extensionAttributes = $this->getOrderExtensionDependency();
        }

        $extensionAttributes->setGiftCardNumber($order->getGiftCardNumber());
        $extensionAttributes->setGiftCardValue('-' . $order->getGiftCardValue());
        $order->setExtensionAttributes($extensionAttributes);

        return $order;
    }

    private function getOrderExtensionDependency()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Sales\Api\Data\OrderExtension');
    }

    public function afterGetList(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Model\ResourceModel\Order\Collection $resultOrder
    )
    {
        foreach ($resultOrder->getItems() as $order) {
            $this->afterGet($subject, $order);
        }
        return $resultOrder;
    }
}
