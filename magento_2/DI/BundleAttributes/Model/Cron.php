<?php

namespace DI\BundleAttributes\Model;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class Cron extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

    public function run()
    {

        $this->updateSums();
        $this->updateBundleQty();

    }

    private function updateSums()
    {
        $simplesValuesSql =
            "SELECT cpbs.product_id, cpbs.parent_product_id, cpbs.selection_qty, cped.value as msrp, cped1.value as map, cped2.value as cost
                FROM `catalog_product_bundle_selection` as cpbs
                LEFT JOIN `catalog_product_entity` as cpe ON  cpbs.parent_product_id = cpe.entity_id
                LEFT JOIN `catalog_product_entity_decimal` as cped ON  cpbs.product_id = cped.row_id AND cped.attribute_id = 117
                LEFT JOIN `catalog_product_entity_decimal` as cped1 ON  cpbs.product_id = cped1.row_id AND cped1.attribute_id = 260
                LEFT JOIN `catalog_product_entity_decimal` as cped2 ON  cpbs.product_id = cped2.row_id AND cped2.attribute_id = 78
                WHERE `cpe`.`type_id` = 'bundle' ORDER BY cpe.entity_id ASC";

        $simplesValues = $this->connection->query($simplesValuesSql)->fetchAll();

        $simplesValuesGroupByParent = [];
        foreach ($simplesValues as $index => $simpleValues) {
            $simplesValuesGroupByParent[$simpleValues['parent_product_id']]['msrp'] += floatval($simpleValues['msrp'] * $simpleValues['selection_qty']);
            $simplesValuesGroupByParent[$simpleValues['parent_product_id']]['map'] += floatval($simpleValues['map'] * $simpleValues['selection_qty']);
            $simplesValuesGroupByParent[$simpleValues['parent_product_id']]['cost'] += floatval($simpleValues['cost'] * $simpleValues['selection_qty']);
        }

        $msrp_sum = 457;
        $map_sum = 460;
        $cost_sum = 459;

        $newSums = [];
        foreach ($simplesValuesGroupByParent as $parentId => $sums) {
            $newSums[] = '(' . $msrp_sum . ', 0,' . $parentId . ',"' . $sums['msrp'] . '")';
            $newSums[] = '(' . $map_sum . ', 0,' . $parentId . ',"' . $sums['map'] . '")';
            $newSums[] = '(' . $cost_sum . ', 0,' . $parentId . ',"' . $sums['cost'] . '")';

        }
        $newParentSums = implode(',', $newSums);

        $this->connection->query("INSERT INTO catalog_product_entity_decimal (attribute_id,store_id, row_id,value) VALUES $newParentSums ON DUPLICATE KEY UPDATE value = VALUES(value);");
    }


    private function updateBundleQty()
    {
        $selections = $this->connection->query(
            "SELECT cpbs.product_id, cpbs.parent_product_id, cpbs.selection_qty, csi.qty
                FROM `catalog_product_bundle_selection` as cpbs
                LEFT JOIN `catalog_product_entity` as cpe ON  cpbs.parent_product_id = cpe.entity_id
                LEFT JOIN `cataloginventory_stock_item` as csi ON  cpbs.product_id = csi.product_id
                WHERE `cpe`.`type_id` = 'bundle' ORDER BY cpe.entity_id ASC")->fetchAll();

        $bundlesSelections = [];
        foreach ($selections as $index => $selection) {
            $selection_qty = intval($selection['selection_qty']);
            if (!$selection_qty) {
                $selection_qty = 1;
            };

            $bundlesSelections[$selection['parent_product_id']][] = round($selection['qty'] / $selection_qty, 0, PHP_ROUND_HALF_DOWN);
        }

        $newQtys = [];
        foreach ($bundlesSelections as $bunbleId => $simplesQty) {
            $newQtys[] = '(' . $bunbleId . ',1,1,' . min($simplesQty) . ')';
        }
        $newParentQty = implode(',', $newQtys);

        $this->connection->query("INSERT INTO cataloginventory_stock_item ( product_id,stock_id, website_id,qty) VALUES $newParentQty ON DUPLICATE KEY UPDATE qty = VALUES(qty);");
    }
}