<?php

namespace DI\ShippingStatus\Observer;

class StatusUpdate implements \Magento\Framework\Event\ObserverInterface
{

    public function __construct(
        \DI\ShippingStatus\Model\Statuses $statuses
    )
    {
        $this->statuses = $statuses;

    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();

        $id = $shipment->getOrderId();

        $order =$shipment->getOrder();

        $order->save();

        $status = $this->statuses->getStatuses($id);

        $order->setData('shipping_status' , $status);
        $order->save();

    }
}