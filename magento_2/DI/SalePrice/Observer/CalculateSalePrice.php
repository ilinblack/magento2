<?php

namespace DI\SalePrice\Observer;

use Magento\Framework\Event\ObserverInterface;

class CalculateSalePrice implements ObserverInterface
{

    public function __construct(
        \DI\SalePrice\Helper\Data $helper,
        \Magento\Catalog\Model\Product $productCollectionFactory

    )
    {
        $this->helper = $helper;
        $this->_productCollectionFactory = $productCollectionFactory;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $product = $observer->getEvent()->getProduct();
            $salePrice = $product->getData('sale_price');


            $_salePriceFrom = $product->getData('special_from_date');
            $_salePriceTo = $product->getData('special_to_date');

            $old_special_price = $product->getSpecialPrice();

            $date = time();

            $price = $product->getData('price');

            if ($product->getTypeId() == 'bundle') {
                $price = $this->getBundleMaxPrice($product);
            }

            $newSpecialPrice = $this->helper->calculateSpecialPrice($price, $salePrice);

            if($newSpecialPrice > 100){
                $newSpecialPrice = 100;
                $product->setData('sale_price', $price);
            }

            if(is_object($_salePriceFrom)){
                $salePriceFrom = $_salePriceFrom->getTimestamp();
            }else{
                $salePriceFrom = strtotime($_salePriceFrom);
            }

            if(is_object($_salePriceTo)){
                $salePriceTo = $_salePriceTo->getTimestamp();
            }else{
                $salePriceTo = strtotime($_salePriceTo);
            }

            if(!$_salePriceFrom) {
                    $product->setData('special_price', $newSpecialPrice);

            }elseif ($_salePriceFrom) {

                if ($salePriceFrom < $date) {
                    $product->setData('special_price', $newSpecialPrice);
                }
            }

            if ($_salePriceTo) {
                if ($salePriceTo < $date) {
                    $product->setData('special_price', 100.000);
                }
            }


            $product->setSpecialPrice(number_format($product->getSpecialPrice(), 4));

            if ($old_special_price != $product->getSpecialPrice()) {
                $product->getResource()->saveAttribute($product, 'special_price');
            }


        }catch (\Exception $e){
        }
    }


    public function getBundleMaxPrice($product)
    {
        $endprice = 0;

        $selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection(
            $product->getTypeInstance(true)->getOptionsIds($product), $product
        );

        foreach ($selectionCollection as $option) {
            $qty = $option->getSelectionQty();
            $_product = $this->_productCollectionFactory->load($option->getId());
            $endprice += $_product->getFinalPrice() * $qty;
        }

        return $endprice;
    }

}