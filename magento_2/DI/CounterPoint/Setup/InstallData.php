<?php
namespace DI\CounterPoint\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'counterpoint_price_1',/* Custom Attribute Code */
            [
                'group' => 'Advanced Pricing',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'decimal',/* Data type in which formate your value save in database*/
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Counter Point Price 1', /* lablel of your attribute*/
                'input' => 'price',
                'class' => 'counterpoint_price',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 999,
                'position' => 999,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'counterpoint_price_2',/* Custom Attribute Code */
            [
                'group' => 'Advanced Pricing',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'decimal',/* Data type in which formate your value save in database*/
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Counter Point Price 2', /* lablel of your attribute*/
                'input' => 'price',
                'class' => 'counterpoint_price',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'counterpoint_price_3',/* Custom Attribute Code */
            [
                'group' => 'Advanced Pricing',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'decimal',/* Data type in which formate your value save in database*/
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Counter Point Price 3', /* lablel of your attribute*/
                'input' => 'price',
                'class' => 'counterpoint_price',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1001,
                'position' => 1001,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'counterpoint_price_4',/* Custom Attribute Code */
            [
                'group' => 'Advanced Pricing',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'decimal',/* Data type in which formate your value save in database*/
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Counter Point Price 4', /* lablel of your attribute*/
                'input' => 'price',
                'class' => 'counterpoint_price',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1002,
                'position' => 1002,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'counterpoint_price_5',/* Custom Attribute Code */
            [
                'group' => 'Advanced Pricing',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'decimal',/* Data type in which formate your value save in database*/
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Counter Point Price 5', /* lablel of your attribute*/
                'input' => 'price',
                'class' => 'counterpoint_price',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1003,
                'position' => 1003,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'counterpoint_price_6',/* Custom Attribute Code */
            [
                'group' => 'Advanced Pricing',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'decimal',/* Data type in which formate your value save in database*/
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Counter Point Price 6', /* lablel of your attribute*/
                'input' => 'price',
                'class' => 'counterpoint_price',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1004,
                'position' => 1004,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );

    }
}