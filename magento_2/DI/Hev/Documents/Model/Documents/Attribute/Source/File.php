<?php

namespace DI\Documents\Model\Documents\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class File extends AbstractSource
{

    public function getAllOptions()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $documentCollection = $objectManager->get('\DI\Documents\Model\ResourceModel\Documents\Collection')
            ->setOrder('doc_name', 'ASC');

        $document_arr = [];

        if($documentCollection->getSize()){
            foreach($documentCollection as $document){
                $document_arr['option'.$document->getId()] = array(
                    'label' => $document->getDocName(),
                    'value' => $document->getId()
                );
            }
        }

        return $document_arr;
    }
}