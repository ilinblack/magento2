<?php

namespace DI\BundleAttributes\Observer;

use Magento\Framework\Event\ObserverInterface;

class BundleAttributes implements ObserverInterface
{
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->_resource = $resource;
        $this->connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product->getTypeId() == 'bundle' && $product->getId()) {
            $id = $product->getId();
            $sql = "SELECT product_id, selection_qty FROM catalog_product_bundle_selection WHERE parent_product_id = $id;";
            $_simpleIds = $this->connection->query($sql);
            $simpleIds = $_simpleIds->fetchAll();

            if ($simpleIds) {
                $attributesArray = [117 => 'msrp_sum', 260 => 'map_sum', 78 => 'cost_sum'];
                foreach ($attributesArray as $key => $attribute) {
                    $this->setAttribute($product, $this->calculateSimpleAttributes($simpleIds, $key), $attribute);
                }
            }

            if ($product->getId() == '142068') {
                $this->updateQty($product, $simpleIds);
            }
        }
    }

    public function calculateSimpleAttributes($simpleIds, $attributeId)
    {
        $attributeValues = [];
        foreach ($simpleIds as $simpleId) {
            $sql = "SELECT value FROM catalog_product_entity_decimal WHERE attribute_id = " . $attributeId . " AND row_id = " . $simpleId['product_id'] . ";";
            $_values = $this->connection->query($sql);
            $attributeValues[] = $_values->fetchColumn() * $simpleId['selection_qty'];
        }
        return $this->sumAttributes($attributeValues);
    }

    private function sumAttributes($values)
    {
        $result = 0;
        foreach ($values as $value) {
            $result += floatval($value);
        }
        return $result;
    }

    private function setAttribute($product, $attributeSum, $attribute)
    {
        if ($product->getData($attribute) != $attributeSum) {
            $product->setData($attribute, $attributeSum);
        }
        $product->getResource()->saveAttribute($product, $attribute);
    }

    private function updateQty($bundle, $simpleIds)
    {
        if (!$simpleIds) return true;

        $newQty = [];
        foreach ($simpleIds as $simpleId) {
            $simpleQty = $this->connection->query("SELECT qty FROM cataloginventory_stock_item WHERE product_id = ' " . $simpleId['product_id'] . "'")->fetchColumn();
            $newQty[] = round($simpleQty / $simpleId['selection_qty'], PHP_ROUND_HALF_DOWN);;
        }
        $newParentQty = min($newQty);
        $this->connection->query("UPDATE cataloginventory_stock_item SET qty = '$newParentQty' WHERE product_id = ' " . $bundle->getId() . "'");
    }
}