<?php

namespace DI\SpecialPrice\Model\Product;

class Price extends \Magento\Bundle\Model\Product\Price{

    /**

     * Rewrite calculateSpecialPrice Method
     *
     * Return FinalPrice

     */

    public function calculateSpecialPrice(
        $finalPrice,
        $specialPrice,
        $specialPriceFrom,
        $specialPriceTo,
        $store = null
    ) {

        if ($specialPrice !== null && $specialPrice != false) {
            if ($this->_localeDate->isScopeDateInInterval($store, $specialPriceFrom, $specialPriceTo)) {
                $finalPrice = min($finalPrice, $specialPrice);
            }
        }

        return $finalPrice;
    }

}