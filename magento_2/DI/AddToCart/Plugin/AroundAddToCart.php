<?php
namespace DI\AddToCart\Plugin;

use Magento\Catalog\Block\Product\AbstractProduct as Subject;

class AroundAddToCart
{

  protected  $_cartHelper = null;
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(\Magento\Catalog\Block\Product\Context $context, array $data = [])
    {
        $this->_cartHelper = $context->getCartHelper();
    }

    public function aroundGetAddToCartUrl($subject,$closure,$product,$additional = [])
    {
        $url = $this->_cartHelper->getAddUrl($product,$additional);
        return $url;
    }
}