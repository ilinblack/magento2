<?php
namespace DI\TransactionsEmailUpc\Controller\Shipment;


class Index extends \Magento\Framework\App\Action\Action{
    protected $_shipmentSender;

    protected $_shipment;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\Order\Email\Sender\ShipmentSender $shipmentSender,
        \Magento\Sales\Model\Order\Shipment $shipment
    )
    {
        parent::__construct($context);
        $this->_shipmentSender = $shipmentSender;
        $this->_shipment = $shipment;
    }
    public function execute(){

        $shipment_id = '6';
        $shipmentData = $this->_shipment->load($shipment_id);
        $result = $this->_shipmentSender->send($shipmentData);

    }

}

