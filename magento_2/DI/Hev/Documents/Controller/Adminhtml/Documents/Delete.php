<?php
namespace DI\Documents\Controller\Adminhtml\Documents;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Delete extends Action
{
    protected $_model;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;


    /**
     * @param Action\Context $context
     * @param \DI\Documents\Model\Documents $model
     */
    public function __construct(
        Action\Context $context,
        \DI\Documents\Model\Documents $model,
        \Magento\Framework\Filesystem $filesystem
    ) {
        parent::__construct($context);
        $this->_model = $model;
        $this->filesystem = $filesystem;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('DI_Documents::docs_delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('doc_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {

            $mediaDirectory = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            $mediaRootDir = $mediaDirectory->getAbsolutePath();
            $docsDir = $mediaRootDir.'tmp/catalog/product';

            try {

                $model = $this->_model;
                $model->load($id);

                if($model->getPath()){
                    unlink($docsDir.$model->getPath());
                }

                $model->delete();

                $this->messageManager->addSuccess(__('Document deleted'));

                return $resultRedirect->setPath('*/*/');
                
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('Document does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}