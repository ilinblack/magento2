<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace DI\ProductDocs\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_1',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 1', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_2',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 2', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_3',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 3', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_4',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 4', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_5',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 5', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_6',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 6', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_7',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 7', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_8',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 8', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_9',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 9', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'test_results_10',/* Custom Attribute Code */
            [
                'group' => 'Product Documents',/* Group name in which you want
                                              to display your custom attribute */
                'type' => 'varchar',/* Data type in which formate your value save in database*/
                'backend' => 'DI\ProductDocs\Model\Attribute\Backend\File',
                'frontend' => '',
                'label' => 'Test Results 10', /* lablel of your attribute*/
                'input' => 'file',
                'class' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'sort_order' => 1000,
                'position' => 1000,
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );
    }
}
