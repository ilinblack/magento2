<?php
namespace  DI\PriceExclusions\Model\ResourceModel\PriceExclusions;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'customer_price_exclusions';
    protected $_eventObject = 'price_exclusions_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('DI\PriceExclusions\Model\PriceExclusions', 'DI\PriceExclusions\Model\ResourceModel\PriceExclusions');
    }

}
