/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Customer store credit(balance) application
 */
/*global define,alert*/
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/error-processor',
        'DI_GiftCards/js/model/payment/giftcards-messages',
        'mage/storage',
        'Magento_Checkout/js/action/get-totals',
        'mage/translate',
        'Magento_Checkout/js/model/payment/method-list'
    ],
    function (
        ko,
        $,
        quote,
        urlManager,
        paymentService,
        errorProcessor,
        messageContainer,
        storage,
        getTotalsAction,
        $t,
        paymentMethodList
    ) {
        'use strict';
        return function (giftcardsValue, isApplied, isLoading) {
            var quoteId = quote.getQuoteId();
            var totals = quote.getTotals();
            var url = 'rest/default/V1/gift-card/apply/'+giftcardsValue;
            var message = $t('Gift Card applied');
            return storage.put(
                url,
                {},
                false
            ).done(
                function (response) {
                    if (response) {
                        quote.setCollectedTotals('giftcards', giftcardsValue);
                        quote.getCalculatedTotal();
                        var deferred = $.Deferred();
                        isLoading(false);
                        isApplied(true);
                        getTotalsAction([], deferred);
                        
                        $.when(deferred).done(function() {
                            
                            if (totals()['grand_total'] == 0) {
                                paymentService.setPaymentMethods(
                                    [{method:"giftcards",title:"Gift Cards"}]
                                );
                            } else {
                                paymentService.setPaymentMethods(
                                    paymentMethodList()
                                );
                            }
                        });
                        messageContainer.addSuccessMessage({'message': message});
                    }
                }
            ).fail(
                function (response) {
                    isLoading(false);
                    errorProcessor.process(response, messageContainer);
                }
            );
        };
    }
);
