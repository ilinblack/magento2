<?php

namespace DI\PriceExclusions\Model\Plugin;

class AfterGetData
{


    public function __construct(\DI\PriceExclusions\Model\PriceExclusionsFactory $priceExclusionsFactory)
    {
        $this->_priceExclusions = $priceExclusionsFactory;
    }


    public function afterGetData(\Magento\Customer\Model\Customer\DataProvider $subject, $result)
    {
        $priceExcl = [];
        $rows = [];

        $customerId = null;
            foreach ($result  as $key => $entity) {
                $customerId = $entity['customer']['entity_id'];

                if ($customerId) {
                    $_collection = $this->_priceExclusions->create();

                    $collection = $_collection->getCollection()->addFilter('customer_id', $customerId)->load();

                    if (count($collection) > 0) {
                        foreach ($collection->getItems() as $row) {

                            $rows[$row->getRecordId()] =
                                [
                                    'record_id' => $row->getRecordId(),
                                    'customer_group_ids' => $row->getCustomerGroupIds(),
                                    'brand_id' => $row->getBrandId()
                                ];

                        }
                    }
                }
                $_result = [];

                foreach($rows as $row){
                    $_result[$row['record_id']] = $row;

                }
                    ksort($_result);
                    $result[$key]['price_excl']['dynamic-rows']['dynamic-rows'] = $_result;

                }

            return $result;
    }
}