<?php
namespace DI\CheckoutAddressField\Model\Plugin\Checkout;
class LayoutProcessor
{
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['address_field']
         = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'options' => [
                    'required'=>true
                ],
                'id' => 'address-field'
            ],
            'dataScope' => 'shippingAddress.finder_address',
            'label' => 'Address',
            'provider' => 'checkoutProvider',
            'required'=>true,

            'visible' => true,
            'validation' => ['required-entry' => true],
            'sortOrder' => 80,
            'id' => 'address-field'
        ];


        return $jsLayout;
    }
}