<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace DI\Documents\Model\ResourceModel\Documents;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'doc_id';

    public function _construct()
    {
        $this->_init(

            'DI\Documents\Model\Documents',
            'DI\Documents\Model\ResourceModel\Documents'
        );
    }
}
