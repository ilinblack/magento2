<?php

namespace DI\CounterPoint\Controller\Adminhtml\GrossProfit;

class Index extends \Magento\Backend\App\Action
{

    /**
     * say admin text
     */

    protected $_backendQuoteSession;
    protected $resultJsonFactory;
    protected $helper;
    protected $_pricingHelper;
    protected $_product;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\Session\Quote $backendQuoteSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \DI\CounterPoint\Helper\Data $helper,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Catalog\Model\Product $product

    )
    {
        $this->helper = $helper;
        $this->_backendQuoteSession = $backendQuoteSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_pricingHelper = $pricingHelper;
        $this->_product = $product;

        parent::__construct($context);
    }

    public function execute()
    {

        $this->_backendQuoteSession->setData('mr_customprice_calculating', true);
        $data = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        $orderQuote = $this->_backendQuoteSession->getQuote();
        $resultBundles = [];
        $resultPrice = [];
        foreach ($orderQuote->getAllItems() as $item) {
            $product = $item->getProduct();

            $cost = $product->getData('cost');
            $price = $product->getData('price');

            if($product->getSpecialPrice() && $product->getTypeId() !== 'bundle' ){
                if( $product->getData('special_to_date') >= date('Y-m-d') || !$product->getData('special_to_date')){
                    $price = $product->getData('special_price');
                }
            }

            if(!$item->getParentItem() && $product->getSpecialPrice()){
                if( $product->getData('special_to_date') >= date('Y-m-d') || !$product->getData('special_to_date')){
                    $price = $product->getData('special_price');
                }
            }

            if ($item->getParentItem()) {


                $parentProduct = $item->getParentItem()->getProduct();
                if ($parentProduct->getSpecialPrice()) {
                    if( $parentProduct->getData('special_to_date') >= date('Y-m-d') || !$parentProduct->getData('special_to_date') ) {
                        $price =  $price * ($parentProduct->getSpecialPrice() / 100);
                    }
                }
            }


            if ($product->getTypeId() == 'bundle') {
                $cost = $this->helper->getBundleCost($product);
                $price = $this->helper->getBundlePrice($product);
                if ($product->getSpecialPrice()) {
                    if( $product->getData('special_to_date') >= date('Y-m-d') || !$product->getData('special_to_date') ) {
                        $price =  $price * ($product->getSpecialPrice() / 100);

                    }
                }
            }


            if ($data['gp'] != "") {

                /**
                    CALCULATE price of GrossProfit %
                 */

                if ($item->getParentItemId() == $data['id']) {
                    $item->setGrossProfit($this->helper->calculatePriceOfGrossProfit($cost , $price , $data['gp']));
                    $item->setGrossProfitPercent($data['gp']);
                    $item->getParentItem()->setGrossProfitPercent($data['gp']);
                    $item->setCalculatedCustomPrice(null);
                    $resultBundles[] = $this->helper->calculatePriceOfGrossProfit($cost , $price , $data['gp']);
                }

                if ($item->getId() == $data['id']) {
                    $item->setGrossProfit($this->helper->calculatePriceOfGrossProfit($cost , $price , $data['gp']));
//                    $item->setGrossProfitPercent($data['gp']);
                    $item->setCalculatedCustomPrice(null);
                }

                $resultPrice['price'] = $this->helper->calculatePriceOfGrossProfit($cost, $price, $data['gp']);

            } elseif ($data['custompriceOf'] != "") {

                /**
                    CALCULATE price of Custom %
                 */

                $gpPercent = $this->helper->calculateGrossProfit($cost, $this->helper->calculatePriceOfCustomPrice($price, $data['custompriceOf']));


                if ($item->getParentItemId() == $data['id']) {

                    $item->setCalculatedCustomPrice($this->helper->calculatePriceOfCustomPrice($price , $data['custompriceOf']));

                    $item->setGrossProfit(null);
                    $resultBundles[] = $this->helper->calculatePriceOfCustomPrice($price , $data['custompriceOf']);
                }

                if ($item->getId() === $data['id']) {
                    $item->setGrossProfitPercent($gpPercent);
                    $item->setCalculatedCustomPrice($this->helper->calculatePriceOfCustomPrice($price , $data['custompriceOf']));
                    $item->setGrossProfit(null);
                }

                if ($item->getParentItem()) {
                }
                $resultPrice['price'] = $this->helper->calculatePriceOfCustomPrice($price , $data['custompriceOf']);

            } elseif ($data['attrType'] != "") {

                /**
                    CALCULATE price of Attributes
                 */

                if ($item->getId() == $data['id']) {
                    $product = $this->_product->load($item->getProductId());
                    $cost = $product->getData('cost');
                    $item->setAttributeCustomPrice($product->getData($data['attrType']));
                    $item->setCalculatedCustomPrice(null);
                    $item->setGrossProfit(null);
                    $gpPercent = $this->helper->calculateGrossProfit($cost, $product->getData($data['attrType']));
                    $item->save();
                    $resultPrice['price'] = $product->getData($data['attrType']);
                    $resultPrice['attrType'] = $data['attrType'];
                    $resultPrice['id'] = $data['id'];
                }
            }

            if ($item->getParentItem()) {
                $item->getParentItem()->save();
            }

            $item->save();
        }

        if (count($resultBundles) > 0) {
            $resultPrice['price'] = array_sum($resultBundles);
        }
        return $result->setData($resultPrice);
    }

    /**
     * @return mixed OLD
     */
    public function executeOLD()
    {
        $data = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        $orderQuote = $this->_backendQuoteSession->getQuote();
        $resultBundles = [];
        $resultPrice = [];

        if ($data['gp'] != "") {
            foreach ($orderQuote->getAllItems() as $item) {
                $product = $item->getProduct();
                if ($product->getTypeId() == 'bundle') {
                    continue;
                }

                $cost = $product->getData('cost');
                $price = $product->getPrice();
                if ($sp = $product->getSpecialPrice()) {
                    $price = $sp;
                }

                if ($item->getParentItemId() == $data['id']) {
                    $parentProduct = $item->getParentItem()->getProduct();
                    if ($sp = $parentProduct->getSpecialPrice()) {
                        $price = $price * ($sp / 100);
                    }

                    $item->setGrossProfit($this->helper->calculatePriceOfGrossProfit($cost, $price, $data['gp']));
                    $item->getParentItem()->setGrossProfitPercent($data['gp']);
                    $item->setCalculatedCustomPrice(null);
                    $resultBundles[] = $this->helper->calculatePriceOfGrossProfit($cost * $item->getQty(), $price * $item->getQty(), $data['gp']);

                }

                if ($item->getId() == $data['id']) {
                    $item->setGrossProfit($this->helper->calculatePriceOfGrossProfit($cost, $price, $data['gp']));
                    $item->setCalculatedCustomPrice(null);
                }

                if ($item->getParentItem()) {
                    $item->getParentItem()->save();
                }

                $item->save();
                $resultPrice['price'] = $this->helper->calculatePriceOfGrossProfit($cost, $price, $data['gp']);
            }



            if (count($resultBundles) > 0) {
                $resultPrice['price'] = array_sum($resultBundles);
            }


        } elseif ($data['custompriceOf'] != "") {

            foreach ($orderQuote->getAllItems() as $item) {
                $product = $item->getProduct();
                if ($product->getTypeId() == 'bundle') {
                    continue;
                }

                $price = $product->getPrice();
                if ($sp = $product->getSpecialPrice()) {
                    $price = $sp;
                }

                if ($item->getParentItemId() == $data['id']) {
                    $parentProduct = $item->getParentItem()->getProduct();
                    if ($sp = $parentProduct->getSpecialPrice()) {
                        $price = $price * ($sp / 100);
                    }

                    $item->setCalculatedCustomPrice($this->helper->calculatePriceOfCustomPrice($price, $data['custompriceOf']));

                    $item->setGrossProfit(null);

                    $resultBundles[] = $this->helper->calculatePriceOfCustomPrice($price * $item->getQty(), $data['custompriceOf']);

                } elseif ($item->getId() === $data['id']) {
                    $product = $item->getProduct();
                    $price = $product->getPrice();
                    $cost = $product->getData('cost');

                    $item->setCalculatedCustomPrice($this->helper->calculatePriceOfCustomPrice($price, $data['custompriceOf']));

                    $gpPercent = $this->helper->calculateGrossProfit($cost, $this->helper->calculatePriceOfCustomPrice($price, $data['custompriceOf']));

                    $item->setGrossProfit(null);
                    if ($item->getParentItem()) {
                        $item->getParentItem()->setGrossProfitPercent($gpPercent);
                    }
                }

                if ($item->getParentItem()) {
                    $item->getParentItem()->save();
                }
                $item->save();

                $resultPrice['price'] = $this->helper->calculatePriceOfCustomPrice($price, $data['custompriceOf']);
            }

            if (count($resultBundles) > 0) {
                $resultPrice['price'] = array_sum($resultBundles);
            }

        } elseif ($data['attrType'] != "") {

            foreach ($orderQuote->getAllVisibleItems() as $item) {
                if ($item->getId() == $data['id']) {

                    $product = $this->_product->load($item->getProductId());
                    $cost = $product->getData('cost');

                    $item->setAttributeCustomPrice($product->getData($data['attrType']));
                    $item->setCalculatedCustomPrice(null);
                    $item->setGrossProfit(null);

                    $gpPercent = $this->helper->calculateGrossProfit($cost, $product->getData($data['attrType']));

                    $item->save();
                    $resultPrice['price'] = $product->getData($data['attrType']);
                    $resultPrice['attrType'] = $data['attrType'];
                    $resultPrice['id'] = $data['id'];

                }
            }
        }

        return $result->setData($resultPrice);

    }

}