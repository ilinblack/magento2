<?php

namespace DI\AddToCart\Block;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    public function getAddToCartUrl($product, $additional = [])
    {
        return $this->_cartHelper->getAddUrl($product, $additional);
    }
}