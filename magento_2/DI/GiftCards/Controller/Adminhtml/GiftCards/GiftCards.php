<?php

namespace DI\GiftCards\Controller\Adminhtml\GiftCards;

class GiftCards extends \Magento\Backend\App\Action
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \DI\GiftCards\Helper\Data $giftCardHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Backend\Model\Session\Quote $backendQuoteSession,
        \DI\GiftCards\Model\GiftCards $giftCards

    )
    {
        $this->_giftCardHelper = $giftCardHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_backendQuoteSession = $backendQuoteSession;
        $this->_giftCards = $giftCards;

        parent::__construct($context);

    }

    public function execute()
    {
        if (!$this->getRequest()->isAjax()) {
            return false;
        }

        $data = $this->getRequest()->getParams();
        $result = $this->resultJsonFactory->create();
        $response = [];

            if(array_key_exists ('giftCardNumber', $data)){
              $response = $this->checkCardValue($data['giftCardNumber']);
            }else if(array_key_exists ('value', $data)){
              $response = $this->useCardValue($data);
            }

            $result->setData($response);

            return $result;


    }

    protected function checkCardValue($giftCardNumber){
        try {
            if(!$giftCardNumber){
                 return ['response' => 'Please,Enter Gift Card Number.',
                    'class' => 'error'];
            }
            $response = $this->_giftCardHelper->getGiftCardBalance($giftCardNumber);
            if ($response) {
                $this->_backendQuoteSession->setData('gift_card_check_value', $response);
                $this->_backendQuoteSession->setData('gift_card_number', $giftCardNumber);


                $grandTotal = $this->_backendQuoteSession->getQuote()->getSubtotal();

                $_redeemValue = $grandTotal;

                if($response < $grandTotal){
                    $_redeemValue = $response;
                }


                return ['response' => $response,
                        'redeemValue' => $_redeemValue];
            }else{
                throw new \Exception('Sorry. Something went wrong. Please try later.');
            }


        }catch (\Exception $ex){
            $this->_backendQuoteSession->setData('gift_card_check_value', '');
            $this->_backendQuoteSession->setData('gift_card_number', '');

            return ['response' => $ex->getMessage(),
                'class' => 'error'];
        }

    }

    protected function useCardValue($data){

        $value  = $data['value'];

        $giftCardCheckValue =  $this->_backendQuoteSession->getData('gift_card_check_value');
        $giftCardNumber =  $this->_backendQuoteSession->getData('gift_card_number');
        $quote = $this->_backendQuoteSession->getQuote();

        if(array_key_exists ('email', $data) && $data['email']) {
            $email = $data['email'];
            $customer = $this->getCustomer($email);
            $this->_giftCardHelper->setGiftcardValueToCustomer($customer, $giftCardCheckValue, $giftCardNumber);

        }else{
            return ['response' => 'Specify Customer Email Please',
                    'class' => 'error'];
        }



        if($value == 0){
            $quote->setData('gift_card_value', $value);
            $this->_backendQuoteSession->setData('gift_card_success', false);
            $quote->save();

            return false;
        }

        if($giftCardCheckValue >= $value) {

            $quote->setData('gift_card_value', $value);
            $this->_backendQuoteSession->setData('gift_card_success', true);
            $this->_backendQuoteSession->setData('gift_card_value', $value);
            $customer->setData('gift_card_number', $giftCardNumber);


            $quote->save();

            $customer->save();
            return ['response' => 'You Use '.$value.' value Of '.$giftCardNumber.' Gift Card',
                    'class' => 'success'];

        }else{
            return ['response' => 'You Can`t Use Value Greater Than The Balance Of Your Gift Card',
                    'class' => 'error'];
        }

    }


    protected function getCustomer($email){



        return $this->_giftCardHelper->getCustomer($email);
    }

}