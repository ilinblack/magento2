<?php

namespace DI\CategoryImageSwap\Observer;

use Magento\Framework\Event\ObserverInterface;


class CollectionLoadAfter implements ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $productCollection = $observer->getEvent()->getCollection();

        $productCollection
            ->addAttributeToSelect('*')
            ->addMediaGalleryData();

        return $this;
    }
}