<?php

namespace DI\CustomersImport\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

if (!function_exists("mb_str_replace")) {
    function mb_str_replace($needle, $replace_text, $haystack)
    {
        $haystack = preg_replace("/[^a-zA-ZА-Яа-я0-9,.—\s]/", "", $haystack);
        return implode($replace_text, mb_split($needle, $haystack));
    }
}

class Import extends Command
{
    protected  $_resource;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;


    protected $_addressFactory;

    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory    $customerFactory
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->_addressFactory = $addressFactory;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->directoryList = $directoryList;
        $this->state = $state;
        $this->_resource = $resource;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('mr:customer_imp');
        $this->setDescription('Import Customers from file');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode('frontend');


        $this->createNewCustomer($this->readCSV('customers/customers.csv'));

        die('customers_imp');
    }


    private function readCSV($csvFile){
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);

        $data = [];

        $dirtyData = [];

        foreach ($line_of_text as $lines){

            $line = explode(";", $lines[0]);

            if(count($line) < 6 ){
                $dirtyData[] = $line;
                continue;
            }



            $data[] = [
                'gsp'=>$line[0],
                'ssn'=>$line[1],
                'ssph'=>$line[2],
                'street'=>$line[3],
                'district'=>$line[4],
                'city'=>$line[5],
                'state'=>$line[6],
                'postcode'=>$line[7]
            ];
        }

        $cleanData = [];

        foreach ($dirtyData as $line) {

            if(count($line) <  2 ){
                continue;
            }
            $cleanData[] = [
                'gsp'=>$line[0],
                'ssn'=>$line[1],
                'ssph'=>$line[2],
                'street'=>$line[3],
                'district'=>"",
                'city'=>"",
                'state'=>"",
                'postcode'=>""
            ];
        }


        for($i=0;$i < count($cleanData); $i++) {
            $data[] = $cleanData[$i];
        }

        unset($data[0]); //cut general information

        return $data;
    }

    private function createNewCustomer($data){

        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();

        $i = 1;
        foreach ($data as $line) {
            // Instantiate object (this is the most important part)



            if(empty($line['street'])){
                $line['street'] = 'street';
            }
            if(empty($line['ssph'])){
                $line['ssph'] = '0987654321';
            }
            if (empty($line['city'])){
                $line['city'] = 'City';
            }
            if (empty($line['state'])){
                $line['state'] = 'State';
            }
            if (empty($line['postcode'])){
                $line['postcode'] = 'PostCode';
            }
            if (empty($line['gsp'])){
                $line['gsp'] = 'gsap';
            }
            if (empty($line['district'])){
                $line['district'] = 'District';
            }
            if ($line['gsp'] == " " ){
                $line['gsp'] = 'gsap';
            }


            $customer   = $this->customerFactory->create();

            $customer->setWebsiteId($websiteId);
            $customer->setGroupId(1);
            // Preparing data for new customer
            $customer->setEmail($line['gsp']."@examplemail".$i.".com");
            $customer->setFirstname($line['gsp']);
            $customer->setLastname($line['gsp']);
            $customer->setStreet($line['street']);
            $customer->setGsap($line['gsp']);
            $customer->setIsCustomerDefined(0);
            $customer->setServiceStationName($line['ssn']);
            $customer->setServiceStationPhone($line['ssph']);

            // Save data
            $customer->save();

            $address = $this->_addressFactory->create();
            $address->setCustomerId($customer->getId())
                ->setFirstname($line['gsp'])
                ->setLastname($line['gsp'])
                ->setCountryId('GB')
                ->setStreet($line['street'])
                ->setCity($line['city'])
                ->setTelephone($line['ssph'])
                ->setPostcode($line['postcode'])
                ->setRegion($line['state'])
                ->setDistrict($line['district'])
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');

            $address->save();


            $i++;
        }

    }

}