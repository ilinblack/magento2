<?php

namespace DI\TransactionsEmailUpc\Block\Order\Email\Items;

class DefaultItems extends \Magento\Sales\Block\Order\Email\Items\DefaultItems{

    public function getUpc($item)
    {

        if($item->getOrderItem()->getProduct()->getTypeId() === 'simple'){
            return $item->getOrderItem()->getProduct()->getUpc();
        }elseif ($item->getOrderItem()->getProduct()->getTypeId() === 'configurable'){
            $upc = null;
            $simples = $item->getOrderItem()->getProduct()->getTypeInstance()->getUsedProducts($item->getOrderItem()->getProduct());
            foreach($simples as $simple ){
                $upc = $simple->getUpc();
                return $upc;
            }
            return $upc;
        }

    }
}