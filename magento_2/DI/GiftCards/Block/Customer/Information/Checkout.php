<?php

namespace DI\GiftCards\Block\Customer\Information;

class Checkout extends \Magento\Framework\View\Element\Template
{
    public function getRegisterUrl()
    {
        return $this->getUrl('customer/account/create');
    }
}