<?php
namespace DI\SalePrice\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{


    public function calculateSpecialPrice($oldPrice, $newPrice){

        if($oldPrice && $newPrice) {

            $newPrice = str_replace(',', "", $newPrice);
            $specialPrice = (((float)100 * (float)$newPrice) / (float)$oldPrice);
            return $specialPrice;

        }
    }

    public function calculateSalePrice($oldPrice, $specialPrice){

        if($oldPrice && $specialPrice) {

            $salePrice = (((float)$oldPrice * (float)$specialPrice) / (float)100);
            return $salePrice;

        }
    }

}