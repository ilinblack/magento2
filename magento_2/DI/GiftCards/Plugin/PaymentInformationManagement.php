<?php

namespace DI\GiftCards\Plugin;

class PaymentInformationManagement
{
    protected $giftCardHelper;
    protected $giftCardModel;
    protected $cart;
    protected $cartManagement;
    protected $quoteRepository;

    public function __construct(
        \DI\GiftCards\Helper\Data $giftCardHelper,
        \DI\GiftCards\Model\GiftCards $giftCardModel,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    )
    {
        $this->giftCardHelper = $giftCardHelper;
        $this->giftCardModel = $giftCardModel;
        $this->cart = $cart;
        $this->cartManagement = $cartManagement;
        $this->quoteRepository = $quoteRepository;
    }

    public function beforeSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress
    )
    {
        $quote = $this->quoteRepository->getActive($cartId);
        $giftcardValue = $quote->getData('gift_card_value');
        $customerGiftcardValue = $this->giftCardHelper->getCurrentCustomerGiftCardBalance();

        if ($giftcardValue > 0) {
            if ($giftcardValue > $customerGiftcardValue) {
                $this->cart->getQuote()->setData('gift_card_value', 0)->save();
                $this->cart->getQuote()->collectTotals()->save();

                throw new CouldNotSaveException(__('Gift Card Amount Mismatch. Please refresh the page.'));
            }
        }
        return [$cartId, $paymentMethod, $billingAddress];
    }
}